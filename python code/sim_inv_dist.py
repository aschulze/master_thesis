from PIL import Image
from skimage.transform import resize
from mpl_toolkits.mplot3d import axes3d
from scipy.interpolate import RegularGridInterpolator, griddata
import numpy as np
import numpy.linalg as nla
import utils as utils
import matplotlib.pyplot as plt

import IMED


def lp_interpolate(img, N, M):
    h, w = img.shape
    x = np.arange(0.0, w)
    y = np.arange(0.0, h)
    img_interpolator = RegularGridInterpolator(
        (y, x), img[::-1, :], bounds_error=False, fill_value=0.0
    )

    # compute new (log-polar) sample points:
    r = np.linspace(0.0, 2.0 * np.pi, num=M, endpoint=False)  # = radii
    t = np.linspace(0.0, 2.0 * np.pi, num=N, endpoint=False)  # = thetas
    R, T = np.meshgrid(r, t, indexing="ij")
    rad = 0.5 * (min(w, h))
    beam = np.exp(R) / np.exp(2*np.pi) * rad
    X = beam * np.cos(T) + 0.5 * w
    Y = beam * np.sin(T) + 0.5 * h

    # compute image value at sample points
    points_log_polar = np.concatenate(
        (Y.reshape(-1, 1), X.reshape(-1, 1)), axis=1)

    return img_interpolator(points_log_polar).reshape((N, M))


def i_lp_interpolate(img, N, M):
    K, V = img.shape

    t = np.linspace(0.0, 2.0 * np.pi, num=K, endpoint=False)
    r = np.linspace(0.0, 2.0 * np.pi, num=V, endpoint=False)

    R, T = np.meshgrid(r, t, indexing="ij")
    rad = 0.5 * (min(N, M))
    beam = np.exp(R) / np.exp(2*np.pi) * rad
    X = beam * np.cos(T) + N / 2
    Y = beam * np.sin(T) + M / 2

    # compute image value at sample points:
    points_log_polar = np.concatenate(
        (Y.reshape(-1, 1), X.reshape(-1, 1)), axis=1)

    Y, X = np.mgrid[0:N, 0:M]  # grid points for reconstructed image
    img = griddata(
        points_log_polar, img.flat[:], (Y, X), method="cubic", fill_value=0.0
    )
    img = resize(img, (N, M), anti_aliasing=True)

    return np.flip(img, 0)


def afmt(fhat, sigma):
    N, M = fhat.shape
    n = np.linspace(0.0, 2.0 * np.pi, num=N, endpoint=False)

    # Shift zero frequency to the center, such that the standard fft layout is achieved.
    ones_n = np.empty((N))
    ones_n[::2] = 1
    ones_n[1::2] = -1
    ones_m = np.empty((M))
    ones_m[::2] = 1
    ones_m[1::2] = -1

    fhat_fft = (
        fhat
        * 2.0
        * np.pi
        / M
        * np.exp(sigma * n[:, None])
        * ones_n[:, None]
        * ones_m[None, :]
    )
    Mf_fft = np.fft.fft2(fhat_fft.T)

    # Add another column/ row for symmetry.
    Mf = np.empty((M + 1, N + 1), dtype="complex128")
    Mf[:M, :N] = Mf_fft
    Mf[:M, N] = Mf_fft[:, 0]
    Mf[M, :N] = Mf_fft[0, :]
    Mf[M, N] = Mf_fft[0, 0]

    return Mf


def iafmt(Mf, sigma):
    M, N = Mf.shape
    K = M // 2
    V = N // 2

    k = np.arange(-K, K + 1)
    v = np.arange(-V, V + 1)

    r = np.linspace(0.0, 2.0 * np.pi, num=M, endpoint=False)
    t = np.linspace(0.0, 2.0 * np.pi, num=N, endpoint=False)

    fhat = np.empty((M - 1, N - 1))
    for n in np.arange(N-1):
        for m in np.arange(M-1):
            fhat[n, m] = np.abs(
                np.sum(Mf * np.exp(1.0j * (k[:, None]*t[m] + v[None, :]*r[n])))
            )

    fhat = fhat * np.exp(-sigma * r[:M-1, None])
    fhat -= np.min(fhat)
    fhat /= np.max(fhat)  # Assumes the original image was scaled tp [0,1]

    return fhat


def descriptor(Mf, sigma):
    N, M = Mf.shape
    K = N // 2
    V = M // 2

    k = np.arange(-K, K + 1)
    v = np.arange(-V, V + 1)

    Mf00 = Mf[K, V]
    Mf10 = Mf[K - 1, V]

    if Mf00 != 0.0:
        If = Mf * np.exp(
            np.log(Mf00) * (-1.0 + 1.0j * v[None, :] / sigma)
            + 1.0j * np.angle(Mf10) * k[:, None]
        )
    else:
        If = Mf
        If[:, :] = 0.0  # TODO: 0.0 should be the best compromise between not making the distance to small, 
        #                 # which leads to problems when assigning patches and making the distance too high,
        #                 # which leads to broken clustering.

    If[np.isnan(If)] = 0.0

    return If


def distance(If1, If2, m=None, n=None):
    if m == None or n == None:
        return np.sqrt(np.sum(np.square(np.abs(If1 - If2))))
    else:
        M, N = If1.shape
        If1_sub = If1[
            M // 2 - m // 2: M // 2 + m // 2, N // 2 - n // 2: N // 2 + n // 2
        ]
        If2_sub = If2[
            M // 2 - m // 2: M // 2 + m // 2, N // 2 - n // 2: N // 2 + n // 2
        ]
        return np.sqrt(np.sum(np.square(np.abs(If1_sub - If2_sub))))


def sim_param(Mf1, Mf2, sigma):
    k0 = Mf1.shape[0] // 2
    v0 = Mf1.shape[1] // 2

    # Check for black images.
    if Mf1[k0 - 1, v0] != 0.0:
        alpha_sigma = np.real(Mf2[k0, v0] / Mf1[k0, v0])
        alpha = np.power(alpha_sigma, -1.0 / sigma)
        beta = np.imag(-np.log(Mf2[k0 - 1, v0] / Mf1[k0 - 1, v0]))
    else:
        alpha = 0.0
        beta = 0.0

    return alpha, beta


def sim_param_lq(Mf1, Mf2, sigma):
    (K, V) = Mf1.shape
    k0 = K // 2
    v0 = V // 2
    A = np.empty((K * V, 2), dtype=complex)
    for k in range(K):
        for v in range(V):
            A[v + V * k, 0] = np.complex(-sigma, v - v0)
            A[v + V * k, 1] = np.complex(0, k - k0)
    B = np.log(Mf2 / Mf1).reshape(K * V, 1)
    X = nla.lstsq(A, B, rcond=None)[0]
    alpha = np.real(np.exp(X[0, 0]))
    beta = np.nan_to_num(np.imag(X[1, 0]), 0.0)

    return alpha, beta


def sim_inv_dist_impl(p1, p2, M=32, N=32, sigma=0.5, m=4, n=4, imed=False):
    # g = utils.gain(p1, p2) # TODO: This makes the distance asymmetric!
    g = 1.0
    p1_log = lp_interpolate(g * p1, M, N)
    p2_log = lp_interpolate(p2, M, N)

    M_p1 = afmt(p1_log, sigma)
    M_p2 = afmt(p2_log, sigma)

    I_p1 = descriptor(M_p1, sigma)
    I_p2 = descriptor(M_p2, sigma)
    if imed:
        d = IMED.IMED(I_p1, I_p2)
    else:
        d = distance(I_p1, I_p2, m, n)

    alpha, beta = sim_param(M_p1, M_p2, sigma)
    A, c = utils.affine_matrix(
        scale=alpha, theta=0.0, phi=beta, shift=[0, 0], img_shape=p1.shape
    )

    return d, g, A, c, alpha, beta


def sim_inv_dist(p1, p2, M=32, N=32, sigma=0.5, m=4, n=4, imed=False):
    """Similarity invariant distance for two patches.
    Parameters
    ----------
    p1 : array
        Patch
    p2 : array
        Patch
    M: integer
        Number of angles for log polar interpolation.
    N: integer
        Number of radial sampling points for log polar interpolation.
    sigma: real
        AFMT scale parameter, > 0
    Returns
    -------
    real
        distance of p1, p2
    real
        gain: g*p1 = p2
    array
        similarity transformation
    array
        translation vector: always zero (exists for confromance reasons)
    """

    d, g, A, c, _, _ = sim_inv_dist_impl(p1, p2, M, N, sigma, m, n)

    return d, g, A, c


def sim_inv_dist_param(p1, p2, M=32, N=32, sigma=0.5, m=4, n=4, imed=False):
    d, g, _, _, alpha, beta = sim_inv_dist_impl(p1, p2, M, N, sigma, m, n)

    return d, g, alpha, beta


def greedy_k_center(patches, k, M=32, N=32, sigma=0.5, m=4, n=4, imed=False, init=0):
    emb_patches = utils.embed_patches(patches)
    def func_dist(p1, p2): return sim_inv_dist(p1, p2, M, N, sigma, m, n, imed)

    return utils.greedy_k_center_impl(emb_patches, k, func_dist, init)


# Returns the indices of the nearest neighbors and the affine parameters.
def nearest_neighbor(patches, labels, M=32, N=32, sigma=0.5, m=4, n=4, imed=False):
    emb_patches = utils.embed_patches(patches)
    emb_labels = utils.embed_labels(labels)
    def func_dist(p1, p2): return sim_inv_dist(p1, p2, M, N, sigma, m, n, imed)
    ind, param = utils.nearest_neighbor_impl(
        emb_patches, emb_labels, func_dist)

    return ind, utils.scale_parameters(param, emb_patches.shape[2:], patches.shape[2:])


# Returns scale and rotation as parameters, instead of affine matrix and offset
def nearest_neighbor_param(
    patches, labels, M=32, N=32, sigma=0.5, m=4, n=4, imed=False
):
    emb_patches = utils.embed_patches(patches)
    emb_labels = utils.embed_labels(labels)
    def func_dist(p1, p2): return sim_inv_dist_param(
        p1, p2, M, N, sigma, m, n, imed)

    return utils.nearest_neighbor_impl(emb_patches, emb_labels, func_dist)
