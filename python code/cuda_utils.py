import patchdistances as pd
from skimage.util import montage
from os import path
from os import mkdir
from matplotlib import gridspec
import numpy as np
import matplotlib.pyplot as plt
from IMED import cft
from utils import mse, psnr

# Default parameters for the experiments.
p_shape = (11, 11)
k = 25
lp = 16
fmt = (4, 4)


def plot_results(img, labels, nn, img_rec_median, img_rec_mean, foldername=""):
    clusters = labels.shape[0]
    m = labels.shape[1]
    n = labels.shape[2]

    colors = np.empty((clusters, m, n))
    for i in range(clusters):  # plot color map for labels
        colors[i, :, :] = i / clusters
    colors = montage(colors, padding_width=2)

    if labels.ndim == 3:
        cmap = "gray"
        labels_plt = montage(labels, padding_width=2)
        labels_plt[labels_plt == labels_plt[0, 0]] = 1.0  # white background
    else:
        cmap = None
        labels_plt = montage(labels, padding_width=2, multichannel=True)
        labels_plt[labels_plt[:, :, 0] ==
                   labels_plt[0, 0, 0]] = 1.0  # white background
        labels_plt[labels_plt[:, :, 1] == labels_plt[0, 0, 1]] = 1.0
        labels_plt[labels_plt[:, :, 2] == labels_plt[0, 0, 2]] = 1.0

    plt.figure(figsize=(18, 5))
    gs = gridspec.GridSpec(1, 6, width_ratios=[1, 1, 1, 1, 1, 1])
    plt.subplot(gs[0])
    plt.imshow(img, cmap=cmap)
    plt.title("original")
    plt.axis("off")
    plt.title("Greedy k-center clustering (k=" + str(clusters) + ")")
    plt.subplot(gs[1])
    plt.imshow(colors, cmap="inferno", vmin=0, vmax=1)
    plt.axis("off")
    plt.subplot(gs[2])
    plt.imshow(labels_plt, cmap=cmap)
    plt.axis("off")

    plt.subplot(gs[3])
    plt.imshow(nn, cmap="inferno")
    plt.axis("off")

    plt.subplot(gs[4])
    plt.imshow(img_rec_median, cmap=cmap)
    plt.axis("off")
    plt.title("Reconstructed image (median)")
    plt.title("Reconstructed image (median)")

    plt.subplot(gs[5])
    plt.imshow(img_rec_mean, cmap=cmap)
    plt.axis("off")
    plt.title("Reconstructed image (mean)")
    plt.title("Reconstructed image (mean)")

    if foldername != "":
        if not path.exists(foldername):
            mkdir(foldername)
        f = "plots/" + foldername
        if not path.exists(f):
            mkdir(f)

        plt.imsave(f + "/original.pdf", img, format="pdf", cmap=cmap)
        plt.imsave(f + "/mean_rec.pdf", img_rec_mean, format="pdf", cmap=cmap)
        plt.imsave(f + "/median_rec.pdf", img_rec_median,
                   format="pdf", cmap=cmap)
        plt.imsave(f + "/clusters.pdf", labels_plt, format="pdf", cmap=cmap)

    print("psnr (median): ", psnr(img, img_rec_median))
    print("psnr (mean):   ", psnr(img, img_rec_mean))


def plot_aid(
    img_rec, img_labels, patch_shape, clusters, solver, func, imed, interpolation, solver_labels=None, init=0, foldername=""
):
    if solver_labels == None:
        solver_labels=solver

    aid = pd.affine_inv_dist()

    labels, _ = aid.greedy_k_center(
        image=img_labels,
        patch_shape=patch_shape,
        clusters=clusters,
        first_patch_idx=init,
        solver=solver_labels,
        func_family=func,
        use_imed=imed,
        interpolation=interpolation,
    )

    img_rec_median, nn = aid.reconstruct(
        image=img_rec,
        labels=labels,
        rec_type=pd.rec_t.Median,
        solver=solver,
        func_family=func,
        use_imed=imed,
        interpolation=interpolation,
    )

    img_rec_mean, _ = aid.reconstruct(
        image=img_rec,
        labels=labels,
        rec_type=pd.rec_t.Mean,
        solver=solver,
        func_family=func,
        use_imed=imed,
        interpolation=interpolation,
    )

    plot_results(img_rec, labels, nn, img_rec_median, img_rec_mean, foldername)


def plot_aid_w_translation(
    img_rec, img_labels, patch_shape, clusters, solver, func, imed, interpolation, init=0, foldername=""
):
    aid = pd.affine_inv_dist()

    labels, _ = aid.greedy_k_center(
        image=img_labels,
        patch_shape=patch_shape,
        clusters=clusters,
        first_patch_idx=init,
        solver=solver,
        func_family=func,
        use_imed=imed,
        interpolation=interpolation,
    )

    img_rec_median, nn = aid.reconstruct_w_translation(
        image=img_rec,
        labels=labels,
        rec_type=pd.rec_t.Median,
        solver=solver,
        func_family=func,
        use_imed=imed,
        interpolation=interpolation,
    )

    img_rec_mean, _ = aid.reconstruct_w_translation(
        image=img_rec,
        labels=labels,
        rec_type=pd.rec_t.Mean,
        solver=solver,
        func_family=func,
        use_imed=imed,
        interpolation=interpolation,
    )

    plot_results(img_rec, labels, nn, img_rec_median, img_rec_mean, foldername)


def plot_sid(
    img_rec,
    img_labels,
    patch_shape,
    clusters,
    log_polar_size,
    fmt_shape,
    interpolation,
    init=0,
    foldername="",
):
    sid = pd.similarity_inv_dist()

    labels, _ = sid.greedy_k_center(
        image=img_labels,
        patch_shape=patch_shape,
        clusters=clusters,
        first_patch_idx=init,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
    )

    img_rec_median, nn = sid.reconstruct(
        image=img_rec,
        labels=labels,
        rec_type=pd.rec_t.Median,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
        interpolation=interpolation,
    )

    img_rec_mean, _ = sid.reconstruct(
        image=img_rec,
        labels=labels,
        rec_type=pd.rec_t.Mean,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
        interpolation=interpolation,
    )

    plot_results(img_rec, labels, nn, img_rec_median, img_rec_mean, foldername)


def plot_sid_w_translation(
    img_rec,
    img_labels,
    patch_shape,
    clusters,
    log_polar_size,
    fmt_shape,
    interpolation,
    init=0,
    foldername="",
):
    sid = pd.similarity_inv_dist()

    labels, _ = sid.greedy_k_center(
        image=img_labels,
        patch_shape=patch_shape,
        clusters=clusters,
        first_patch_idx=init,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
    )

    img_rec_median, nn = sid.reconstruct_w_translation(
        image=img_rec,
        labels=labels,
        rec_type=pd.rec_t.Median,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
        interpolation=interpolation,
    )

    img_rec_mean, _ = sid.reconstruct_w_translation(
        image=img_rec,
        labels=labels,
        rec_type=pd.rec_t.Mean,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
        interpolation=interpolation,
    )

    plot_results(img_rec, labels, nn, img_rec_median, img_rec_mean, foldername)


def plot_sid_w_aid_labels(
    img, patch_shape, clusters, log_polar_size, fmt_shape, interpolation, foldername=""
):
    aid = pd.affine_inv_dist()

    labels, _ = aid.greedy_k_center(
        image=img,
        patch_shape=patch_shape,
        clusters=clusters,
        first_patch_idx=0,
        solver=pd.solver_t.procrustes,
        func_family=pd.func_t.superlevelsets,
        use_imed=False,
        interpolation=interpolation,
    )

    sid = pd.similarity_inv_dist()

    img_rec_median, nn = sid.reconstruct(
        image=img,
        labels=labels,
        rec_type=pd.rec_t.Median,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
        interpolation=interpolation,
    )

    img_rec_mean, nn = sid.reconstruct(
        image=img,
        labels=labels,
        rec_type=pd.rec_t.Mean,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
        interpolation=interpolation,
    )

    plot_results(img, labels, nn, img_rec_median, img_rec_mean, foldername)


def plot_sid_w_translation_w_aid_labels(
    img, patch_shape, clusters, log_polar_size, fmt_shape, interpolation, foldername=""
):
    aid = pd.affine_inv_dist()

    labels, _ = aid.greedy_k_center(
        image=img,
        patch_shape=patch_shape,
        clusters=clusters,
        first_patch_idx=0,
        solver=pd.solver_t.procrustes,
        func_family=pd.func_t.superlevelsets,
        use_imed=False,
        interpolation=interpolation,
    )

    sid = pd.similarity_inv_dist()

    img_rec_median, nn = sid.reconstruct_w_translation(
        image=img,
        labels=labels,
        rec_type=pd.rec_t.Median,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
        interpolation=interpolation,
    )

    img_rec_mean, nn = sid.reconstruct_w_translation(
        image=img,
        labels=labels,
        rec_type=pd.rec_t.Mean,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
        interpolation=interpolation,
    )

    plot_results(img, labels, nn, img_rec_median, img_rec_mean, foldername)


def plot_aid_w_sid_labels(img, patch_shape, clusters, solver, func, imed, interpolation, foldername=""):
    sid = pd.similarity_inv_dist()

    labels, _ = sid.greedy_k_center(
        image=img,
        patch_shape=patch_shape,
        clusters=clusters,
        first_patch_idx=0,
        log_polar_size=32,
        fmt_shape=(4, 4),
        sigma=0.5,
    )

    aid = pd.affine_inv_dist()

    img_rec_median, nn = aid.reconstruct(
        image=img,
        labels=labels,
        rec_type=pd.rec_t.Median,
        solver=solver,
        func_family=func,
        use_imed=imed,
        interpolation=interpolation,
    )

    img_rec_mean, nn = aid.reconstruct(
        image=img,
        labels=labels,
        rec_type=pd.rec_t.Mean,
        solver=solver,
        func_family=func,
        use_imed=imed,
        interpolation=interpolation,
    )

    plot_results(img, labels, nn, img_rec_median, img_rec_mean, foldername)


def plot_aid_w_translation_w_sid_labels(img, patch_shape, clusters, solver, func, imed, interpolation, foldername=""):
    sid = pd.similarity_inv_dist()

    labels, _ = sid.greedy_k_center(
        image=img,
        patch_shape=patch_shape,
        clusters=clusters,
        first_patch_idx=0,
        log_polar_size=16,
        fmt_shape=(4, 4),
        sigma=0.5,
    )

    aid = pd.affine_inv_dist()

    img_rec_median, nn = aid.reconstruct_w_translation(
        image=img,
        labels=labels,
        rec_type=pd.rec_t.Median,
        solver=solver,
        func_family=func,
        use_imed=imed,
        interpolation=interpolation,
    )

    img_rec_mean, nn = aid.reconstruct_w_translation(
        image=img,
        labels=labels,
        rec_type=pd.rec_t.Mean,
        solver=solver,
        func_family=func,
        use_imed=imed,
        interpolation=interpolation,
    )

    plot_results(img, labels, nn, img_rec_median, img_rec_mean, foldername)


def plot_results_images(img1, img2, rec1, rec2, foldername):
    diff1 = np.linalg.norm(rec1 - img1, 2, axis=2)
    diff1_cft = np.linalg.norm(cft(rec1)-cft(img1), 2, axis=2)
    diff2 = np.linalg.norm(rec2 - img2, 2, axis=2)
    diff2_cft = np.linalg.norm(cft(rec2)-cft(img2), 2, axis=2)

    diff1_dist = np.linalg.norm(diff1, 2)
    diff1_cft_dist = np.linalg.norm(diff1_cft, 2)
    diff2_dist = np.linalg.norm(diff2, 2)
    diff2_cft_dist = np.linalg.norm(diff2_cft, 2)

    plt.figure()
    plt.subplot(2, 4, 1)
    plt.imshow(img1)
    plt.title("Image")
    plt.subplot(2, 4, 2)
    plt.imshow(rec1)
    plt.title("Reconstruction")
    plt.subplot(2, 4, 3)
    plt.imshow(diff1, cmap="bwr")
    plt.title("Rec Diff")
    plt.subplot(2, 4, 4)
    plt.imshow(diff1_cft, cmap="bwr")
    plt.title("Rec Diff CFT")
    plt.subplot(2, 4, 5)
    plt.imshow(img2)
    plt.subplot(2, 4, 6)
    plt.imshow(rec2)
    plt.subplot(2, 4, 7)
    plt.imshow(diff2, cmap="bwr")
    plt.title("{:5.2f}\n{:5.2f}".format(diff1_dist, diff2_dist))
    plt.subplot(2, 4, 8)
    plt.imshow(diff2_cft, cmap="bwr")
    plt.title("{:5.2f}\n{:5.2f}".format(diff1_cft_dist, diff2_cft_dist))

    if foldername != "":
        if not path.exists(foldername):
            mkdir(foldername)

        if img1.ndim == 2:
            plt.imsave(foldername + "/rec1.pdf", rec1,
                       format="pdf", cmap="gray")
            plt.imsave(foldername + "/rec2.pdf", rec2,
                       format="pdf", cmap="gray")
        else:
            plt.imsave(foldername + "/rec1.pdf", rec1, format="pdf")
            plt.imsave(foldername + "/rec2.pdf", rec2, format="pdf")

        plt.imsave(
            foldername + "/diff1.pdf",
            diff1,
            format="pdf",
            cmap="bwr"
        )
        plt.imsave(
            foldername + "/diff2.pdf",
            diff2,
            format="pdf",
            cmap="bwr"
        )
        plt.imsave(
            foldername + "/diff_cft1.pdf",
            diff1_cft,
            format="pdf",
            cmap="bwr"
        )
        plt.imsave(
            foldername + "/diff_cft2.pdf",
            diff2_cft,
            format="pdf",
            cmap="bwr"
        )


def plot_aid_images(img1, img2, solver, func, foldername=""):
    aid = pd.affine_inv_dist()

    rec1, _ = aid.reconstruct(
        image=img1,
        labels=[img2],
        rec_type=pd.rec_t.Median,
        solver=solver,
        func_family=func,
        use_imed=False,
    )

    rec2, _ = aid.reconstruct(
        image=img2,
        labels=[img1],
        rec_type=pd.rec_t.Median,
        solver=solver,
        func_family=func,
        use_imed=False,
    )

    plot_results_images(img1, img2, rec1, rec2, foldername)


def plot_sid_images(img1, img2, log_polar_size, fmt_shape, interpolation, foldername=""):
    sid = pd.similarity_inv_dist()

    rec1, _ = sid.reconstruct(
        image=img1,
        labels=[img2],
        rec_type=pd.rec_t.Median,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
        interpolation=interpolation,
    )

    rec2, _ = sid.reconstruct(
        image=img2,
        labels=[img1],
        rec_type=pd.rec_t.Median,
        log_polar_size=log_polar_size,
        fmt_shape=fmt_shape,
        sigma=0.5,
        interpolation=interpolation,
    )

    plot_results_images(img1, img2, rec1, rec2, foldername)

# Plots a graph of the mean squared error for the reconstructed images by all methods.


def plot_error_by_atoms(img, min, max, steps, filename=""):
    aid = pd.affine_inv_dist()
    sid = pd.similarity_inv_dist()

    clusters_arr = np.linspace(min, max, num=steps, dtype=int)

    err_ident = [None]*steps
    err_ident_imed = [None]*steps
    err_least = [None]*steps
    err_proc = [None]*steps
    err_afmt = [None]*steps

    err_ident_wt = [None]*steps
    err_ident_imed_wt = [None]*steps
    err_least_wt = [None]*steps
    err_proc_wt = [None]*steps
    err_afmt_wt = [None]*steps

    labels_ident, _ = aid.greedy_k_center(
        image=img,
        patch_shape=p_shape,
        clusters=max,
        first_patch_idx=0,
        solver=pd.solver_t.identity,
        func_family=pd.func_t.superlevelsets,
        use_imed=False,
        interpolation=pd.inter_t.bicubic,
    )

    labels_ident_imed, _ = aid.greedy_k_center(
        image=img,
        patch_shape=p_shape,
        clusters=max,
        first_patch_idx=0,
        solver=pd.solver_t.identity,
        func_family=pd.func_t.superlevelsets,
        use_imed=True,
        interpolation=pd.inter_t.bicubic,
    )

    labels_least, _ = aid.greedy_k_center(
        image=img,
        patch_shape=p_shape,
        clusters=max,
        first_patch_idx=0,
        solver=pd.solver_t.least_squares,
        func_family=pd.func_t.superlevelsets,
        use_imed=False,
        interpolation=pd.inter_t.bicubic,
    )

    labels_proc, _ = aid.greedy_k_center(
        image=img,
        patch_shape=p_shape,
        clusters=max,
        first_patch_idx=0,
        solver=pd.solver_t.procrustes,
        func_family=pd.func_t.superlevelsets,
        use_imed=False,
        interpolation=pd.inter_t.bicubic,
    )

    # Use Procrustes labels instead, since this approach is not reliable.
    # labels_afmt, _ = sid.greedy_k_center(
    #     image=img,
    #     patch_shape=p_shape,
    #     clusters=max,
    #     first_patch_idx=0,
    #     log_polar_size=lp,
    #     fmt_shape=fmt,
    #     sigma=0.5,
    # )
    labels_afmt = labels_proc

    for i, clusters in enumerate(clusters_arr):
        img_ident, _ = aid.reconstruct(
            image=img,
            labels=labels_ident[:clusters],
            rec_type=pd.rec_t.Mean,
            solver=pd.solver_t.identity,
            func_family=pd.func_t.superlevelsets,
            use_imed=False,
            interpolation=pd.inter_t.bicubic,
        )

        # TODO: identical (almost)
        # img_ident_imed, _ = aid.reconstruct(
        #     image=img,
        #     labels=labels_ident[:clusters],
        #     rec_type=pd.rec_t.Mean,
        #     solver=pd.solver_t.identity,
        #     func_family=pd.func_t.superlevelsets,
        #     use_imed=True,
        #     interpolation=pd.inter_t.bicubic,
        # )

        img_least, _ = aid.reconstruct(
            image=img,
            labels=labels_least[:clusters],
            rec_type=pd.rec_t.Mean,
            solver=pd.solver_t.least_squares,
            func_family=pd.func_t.superlevelsets,
            use_imed=False,
            interpolation=pd.inter_t.bicubic,
        )

        img_proc, _ = aid.reconstruct(
            image=img,
            labels=labels_proc[:clusters],
            rec_type=pd.rec_t.Mean,
            solver=pd.solver_t.procrustes,
            func_family=pd.func_t.superlevelsets,
            use_imed=False,
            interpolation=pd.inter_t.bicubic,
        )

        img_afmt, _ = sid.reconstruct(
            image=img,
            labels=labels_afmt[:clusters],
            rec_type=pd.rec_t.Mean,
            log_polar_size=lp,
            fmt_shape=fmt,
            sigma=0.5,
            interpolation=pd.inter_t.bicubic,
        )

        err_ident[i] = mse(img, img_ident)
        # err_ident_imed[i] = mse(img, img_ident_imed)
        err_least[i] = mse(img, img_least)
        err_proc[i] = mse(img, img_proc)
        err_afmt[i] = mse(img, img_afmt)

        # local nearest neighbor
        img_ident_wt, _ = aid.reconstruct_w_translation(
            image=img,
            labels=labels_ident[:clusters],
            rec_type=pd.rec_t.Mean,
            solver=pd.solver_t.identity,
            func_family=pd.func_t.superlevelsets,
            use_imed=False,
            interpolation=pd.inter_t.bicubic,
        )

        # img_ident_imed_wt, _ = aid.reconstruct_w_translation(
        #     image=img,
        #     labels=labels_ident[:clusters],
        #     rec_type=pd.rec_t.Mean,
        #     solver=pd.solver_t.identity,
        #     func_family=pd.func_t.superlevelsets,
        #     use_imed=False,
        #     interpolation=pd.inter_t.bicubic,
        # )

        img_least_wt, _ = aid.reconstruct_w_translation(
            image=img,
            labels=labels_least[:clusters],
            rec_type=pd.rec_t.Mean,
            solver=pd.solver_t.least_squares,
            func_family=pd.func_t.superlevelsets,
            use_imed=False,
            interpolation=pd.inter_t.bicubic,
        )

        img_proc_wt, _ = aid.reconstruct_w_translation(
            image=img,
            labels=labels_proc[:clusters],
            rec_type=pd.rec_t.Mean,
            solver=pd.solver_t.procrustes,
            func_family=pd.func_t.superlevelsets,
            use_imed=False,
            interpolation=pd.inter_t.bicubic,
        )

        img_afmt_wt, _ = sid.reconstruct_w_translation(
            image=img,
            labels=labels_afmt[:clusters],
            rec_type=pd.rec_t.Mean,
            log_polar_size=lp,
            fmt_shape=fmt,
            sigma=0.5,
            interpolation=pd.inter_t.bicubic,
        )

        err_ident_wt[i] = mse(img, img_ident_wt)
        # err_ident_imed_wt[i] = mse(img, img_ident_imed_wt)
        err_least_wt[i] = mse(img, img_least_wt)
        err_proc_wt[i] = mse(img, img_proc_wt)
        err_afmt_wt[i] = mse(img, img_afmt_wt)

    plt.figure()
    plt.xlabel(r"Atoms", fontsize=20)
    plt.ylabel(r"MSE", fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.plot(clusters_arr, err_ident, color="black",
             linestyle="-", label=r"Identity")
    # plt.plot(clusters_arr, err_ident_imed, color="green",
    #          linestyle="-", label=r"Identity (IMED)")
    plt.plot(clusters_arr, err_least, color="black",
             linestyle="--", label=r"Least squares")
    plt.plot(clusters_arr, err_proc, color="black",
             linestyle="-.", label=r"Procrustes")
    plt.plot(clusters_arr, err_afmt, color="black",
             linestyle=":", label=r"AFMT")
    plt.legend(fontsize="xx-large")
    axes = plt.gca()
    axes.set_ylim([0, 0.07])

    if filename != "":
        if not path.exists("plots/"):
            mkdir("plots/")
        plt.savefig("plots/" + filename + ".pdf",
                    format="pdf", bbox_inches='tight')

    plt.figure()
    plt.xlabel(r"Atoms", fontsize=20)
    plt.ylabel(r"MSE", fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.plot(clusters_arr, err_ident_wt, color="black",
             linestyle="-", label=r"Identity")
    # plt.plot(clusters_arr, err_ident_imed_wt, color="green",
    #          linestyle="-", label=r"Identity (IMED)")
    plt.plot(clusters_arr, err_least_wt, color="black",
             linestyle="--", label=r"Least squares")
    plt.plot(clusters_arr, err_proc_wt, color="black",
             linestyle="-.", label=r"Procrustes")
    plt.plot(clusters_arr, err_afmt_wt, color="black",
             linestyle=":", label=r"AFMT")
    plt.legend(fontsize="xx-large")
    axes = plt.gca()
    axes.set_ylim([0, 0.07])

    if filename != "":
        plt.savefig("plots/" + filename + "_w_translation.pdf",
                    format="pdf", bbox_inches='tight')


def aid_distance_matrix(patches1, patches2, func, imed, interpolation):
    return pd.affine_inv_dist().distance_matrix(patches1, patches2, func, imed, interpolation)

# Extracts a patch from an image.


def get_patch(img, X, Y, x, y):
    return img[y: y + Y, x: x + X]


# Mask image with largest contained disk.
def diskify(A):
    out = np.copy(A)
    if A.ndim == 2:
        m, n = A.shape
        m2, n2 = m / 2, n / 2
        r = np.min([m2, n2])

        for i in range(m):
            for j in range(n):
                if np.sqrt(np.square(i - m2) + np.square(j - n2)) > r:
                    out[i, j] = 0
    else:
        m, n, _ = A.shape
        m2, n2 = (m - 1) / 2, (n - 1) / 2
        r = np.min([m2, n2])

        for i in range(m):
            for j in range(n):
                if np.sqrt(np.square(i - m2) + np.square(j - n2)) > r:
                    out[i, j, 0] = 0
                    out[i, j, 1] = 0
                    out[i, j, 2] = 0

    return out
