# %%
from skimage.io import imread
from skimage.transform import resize
from scipy.ndimage import affine_transform
from matplotlib import gridspec
from os import path
from os import mkdir
import numpy as np
import matplotlib.pyplot as plt

from load_images import *
import utils as utils
import sim_inv_dist as sid

#
##
# Thesis Experiments (function definitions)
##
#

def plot_alphas(p1, p2, alpha=0.0, M=32, N=32, sigma=0.5, filename=""):
    p1_log = sid.lp_interpolate(p1, M, N)
    p2_log = sid.lp_interpolate(p2, M, N)

    M_p1 = sid.afmt(p1_log, sigma)
    M_p2 = sid.afmt(p2_log, sigma)

    K = M // 2
    V = N // 2
    v = np.arange(-V, V + 1)
    exp = 1.0 / (-sigma + 1j * v)
    alphas = np.real(np.power(M_p2[K, :] / M_p1[K, :], exp))

    alpha_lq, _ = sid.sim_param_lq(M_p1, M_p2, sigma)

    plt.figure()
    plt.xlabel("v", fontsize=16)
    plt.ylabel(r"$\alpha$", fontsize=16)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    plt.plot(v, [alpha] * v.size, label="reference",
             color="black", linestyle="--")
    plt.plot(v, alphas, label="single frequency", color="black", linestyle="-")
    plt.plot(v, [alpha_lq] * v.size, label="least squares",
             color="black", linestyle=":")
    legend = plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
                     ncol=3, mode="expand", borderaxespad=0.)

    if not filename == "":
        plt.savefig(filename + ".pdf", format="pdf", bbox_inches='tight')


def plot_betas(p1, p2, beta=0.0, M=32, N=32, sigma=0.5, filename=""):
    p1_log = sid.lp_interpolate(p1, M, N)
    p2_log = sid.lp_interpolate(p2, M, N)

    M_p1 = sid.afmt(p1_log, sigma)
    M_p2 = sid.afmt(p2_log, sigma)

    K = M // 2
    V = N // 2
    k = np.arange(-K, K + 1)

    # Beta can not be computed for those values.
    M_p1[K, V] = 1
    M_p2[K, V] = np.inf
    k[K] = 1

    betas = np.imag(np.log(M_p2[:, V] / M_p1[:, V])) / k

    _, beta_lq = sid.sim_param_lq(M_p1, M_p2, sigma)

    plt.figure()
    plt.xlabel("k", fontsize=16)
    plt.ylabel(r"$\beta$ [rad]", fontsize=16)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    plt.plot(k, [beta] * k.size, label="reference",
             color="black", linestyle="--")
    plt.plot(k, betas, label="single frequency", color="black", linestyle="-")
    plt.plot(k, [beta_lq] * k.size, label="least squares",
             color="black", linestyle=":")
    legend = plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
                        ncol=3, mode="expand", borderaxespad=0.)

    if not filename == "":
        plt.savefig(filename + ".pdf", format="pdf", bbox_inches='tight')


def plot_sim_result(
    img, img_sim, img_estim, d, alpha, beta, alpha_estim, beta_estim, fname=""
):
    beta = beta * 180 / np.pi
    beta_estim = (beta_estim * 180 / np.pi) % 360

    plt.figure(figsize=(14, 5))
    plt.subplot(1, 4, 1)
    plt.imshow(img, cmap="gray")
    plt.title("original")
    plt.axis("off")
    plt.subplot(1, 4, 2)
    plt.imshow(img_sim, cmap="gray")
    plt.title(
        "similar \n"
        + "alpha="
        + "{0:.2f}".format(alpha)
        + "\nbeta="
        + "{0:.2f}".format(beta)
    )
    plt.axis("off")
    plt.subplot(1, 4, 3)
    plt.imshow(img_estim, cmap="gray")
    plt.title(
        "approximation \n"
        + "alpha="
        + "{0:.2f}".format(alpha_estim)
        + "\nbeta="
        + "{0:.2f}".format(beta_estim)
    )
    plt.axis("off")
    plt.subplot(1, 4, 4)
    plt.imshow(np.abs(img_sim - img_estim), cmap="Greys")
    plt.title(
        "Fourier distance: " + "{0:.2f}".format(d) + "\n"
        "L2 error: " +
        "{0:.2f}".format(np.sum(abs(img_sim - img_estim), axis=None))
    )
    plt.axis("off")

    if not fname == "":
        plt.savefig(fname + ".pdf", format="pdf")


def plot_sid(img1, img2, log_polar_size, fmt_shape, filename=""):
    # Estimation of img1 by img2.
    d, g, A, c, alpha, beta = sid.sim_inv_dist_impl(
        p1=img1, p2=img2, M=log_polar_size, N=log_polar_size, sigma=0.5, m=fmt_shape[0], n=fmt_shape[1], imed=False)
    img_estim = g * affine_transform(img1, A, offset=c)
    plot_sim_result(img1, img2, img_estim, d, alpha, beta, alpha, beta)

    if filename != "":
        plt.imsave(filename + "_estim.pdf", img_estim,
                   format="pdf", cmap="gray")
        plt.imsave(filename + "_diff.pdf",
                   np.abs(img_estim - img2),
                   format="pdf",
                   cmap="Greys"
                   )


def plot_sid_fmt_shape(img1, img2, img3, filename=""):
    # Embedding images, such that no information is lost on translation.
    img1_ = np.zeros((img1.shape[0] + 100, img1.shape[1] + 100))
    img2_ = np.zeros((img2.shape[0] + 100, img2.shape[1] + 100))
    img3_ = np.zeros((img3.shape[0] + 100, img3.shape[1] + 100))
    img1_[50: 50 + img1.shape[0], 50: 50 + img1.shape[1]] = img1
    img2_[50: 50 + img2.shape[0], 50: 50 + img2.shape[1]] = img2
    img3_[50: 50 + img3.shape[0], 50: 50 + img3.shape[1]] = img3

    kv_arr = np.linspace(0, 64, num=9, dtype="int")  # x-axis: fmt shape
    off_arr = np.linspace(0, 8, num=8, dtype="int")  # graphs: translation

    dist = np.zeros((len(off_arr), len(kv_arr)))  # each row is a graph
    for idy, off in enumerate(off_arr):
        for idx, kv in enumerate(kv_arr):
            A, c = utils.affine_matrix(
                scale=1.0, theta=0.0, phi=0.0, shift=[0, off], img_shape=img1_.shape
            )
            img2_trans = affine_transform(img2_, A, offset=c)
            img3_trans = affine_transform(img3_, A, offset=c)

            dist[idy, idx] = (
                sid.sim_inv_dist_impl(
                    img1_, img3_trans, *img1_.shape, n=kv, m=kv)[0]
                / sid.sim_inv_dist_impl(img1_, img2_trans, *img1_.shape, n=kv, m=kv)[0]
            )

    plt.figure()
    plt.plot(kv_arr, dist[0, :], color="black")
    plt.plot(kv_arr, dist[1, :], color="black")
    plt.plot(kv_arr, dist[2, :], color="black")
    plt.plot(kv_arr, dist[3, :], color="black")
    plt.plot(kv_arr, dist[4, :], color="black")
    plt.plot(kv_arr, dist[5, :], color="black")
    plt.plot(kv_arr, dist[6, :], color="black")
    plt.plot(kv_arr, dist[7, :], color="black")
    plt.xlabel(r"$K = V$", fontsize=16)
    plt.ylabel(r"$\frac{d(a.c)}{d(a.b)}$", fontsize=16)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)

    if filename != "":
        plt.savefig(filename + ".pdf", format="pdf", bbox_inches='tight')


# %%
#
##
# Thesis Experiments
##
#
folder = "../figures/"
plot_alphas(img_butterfly_sim, img_butterfly, 1/1.2, *
            img_butterfly.shape, filename=folder+"afmt_alphas")
plot_betas(img_butterfly_sim, img_butterfly, -0.25 * np.pi,
           *img_butterfly.shape, filename=folder+"afmt_betas")

plot_sid(img1=img_butterfly, img2=img_butterfly_sim,
         log_polar_size=128, fmt_shape=(4, 4), filename=folder+"afmt_ordinary")
plot_sid(img1=img_butterfly, img2=0.75*img_butterfly_sim,
         log_polar_size=128, fmt_shape=(4, 4), filename=folder+"afmt_gain")
plot_sid(img1=img_butterfly_noisy, img2=img_butterfly_sim,
         log_polar_size=128, fmt_shape=(4, 4), filename=folder+"afmt_noisy")

plot_sid_fmt_shape(img_butterfly, img_butterfly_sim,
                   img_butterfly3, filename=folder+"afmt_shape")

# %%
#
##
# Additional Experiments
##
#


# Reconstruction from the Fourier-Mellin coefficients.
alpha = 1.0
beta = 0.5

img = imread("images/butterfly.png", as_gray=True)
A, c = utils.affine_matrix(
    scale=alpha, theta=0.0 * np.pi, phi=beta * np.pi, shift=[0, 0], img_shape=img.shape
)
img_sim = affine_transform(img, A, offset=c)

# Log-polar interpolation
M = 64  # number of angles
N = 64  # number of radial sampling points
sigma = 0.5
Mf = sid.afmt(sid.lp_interpolate(img, M, N), sigma)
img_rec = sid.i_lp_interpolate(
    sid.iafmt(Mf, sigma), img.shape[0], img.shape[1])

plt.figure(figsize=(14, 5))
plt.subplot(1, 3, 1)
plt.imshow(img, cmap="gray")
plt.title("original")
plt.axis("off")
plt.subplot(1, 3, 2)
plt.imshow(img_rec, cmap="gray")
plt.title("reconstructed")
plt.axis("off")
plt.subplot(1, 3, 3)
plt.imshow(img - img_rec, cmap="bwr", vmin=-1.0, vmax=1.0)
plt.title("diff")
plt.axis("off")


def plot_coeff(Mf, title=""):
    # Currently not used.
    w, h = Mf.shape
    K = w / 2
    V = h / 2

    fig = plt.figure()
    ax = fig.gca(projection="3d")
    X, Y = np.mgrid[-K:K, -V:V]
    ax.plot_wireframe(X, Y, np.abs(Mf), rstride=1, cstride=0)
    ax.set_xlabel("k")
    ax.set_ylabel("v")
    ax.set_title(title)

    plt.show()

# %%
