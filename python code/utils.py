from skimage.transform import resize
from enum import Enum
import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage as scipy

def gain(img1, img2):
    div1 = np.sum(np.square(img2))
    div2 = np.sum(np.power(img2, 4))
    if div1 != 0.0 and div2 != 0:
        sum1 = np.sum(np.square(img1)) / div1
        sum2 = np.sum(np.power(img1, 4)) / div2
        g = np.sqrt(sum2 / sum1)
    else:
        g = 0.0

    if not np.isfinite(g):
        g = 0.0  # TODO: Is this the best choice?

    return g

def affine_matrix_helper(scale, theta, phi, shift, img_shape):
    A = scale * np.array(
        [
            [np.cos(theta) * np.cos(phi), -np.sin(phi)],
            [np.cos(theta) * np.sin(phi), np.cos(phi)],
        ]
    )
    center = 0.5 * (np.asarray(img_shape) - 1)
    c = center - A.dot(0.5 * (np.asarray(img_shape) - 1)) - np.asarray(shift)

    return A, c

def affine_matrix(scale, theta, phi, shift, img_shape):
    if len(img_shape) == 2:
        return affine_matrix_helper(scale, theta, phi, shift, img_shape)
    else:
        return affine_matrix_helper(scale, theta, phi, shift, img_shape[0:2])


def affine_transform(img, A, c):
    if img.ndim == 2:
        return np.clip(scipy.affine_transform(img, A, offset=c), 0, 1)
    else:
        ch0 = img[:,:,0]
        ch1 = img[:,:,1]
        ch2 = img[:,:,2]
        ch0_ = scipy.affine_transform(ch0, A, offset=c)
        ch1_ = scipy.affine_transform(ch1, A, offset=c)
        ch2_ = scipy.affine_transform(ch2, A, offset=c)
        out = np.zeros(img.shape)
        out[:,:,0] = ch0_
        out[:,:,1] = ch1_
        out[:,:,2] = ch2_
        return np.clip(out, 0, 1)

def parameters(A):
    alpha = np.linalg.det(A)
    A = A / alpha
    beta = np.arcsin(-A[0, 1])
    theta = np.arccos(A[0, 0] / A[1, 1])

    return alpha, beta, theta


# rescale the affine transform to the coordinates [0,H]x[0,W]:
# offset after scaling!
def scale_transforms(A, c, scale1, scale2, off):
    S1 = np.diag(np.array([scale1, scale1], dtype="float64"))
    S2 = np.diag(np.array([scale2, scale2], dtype="float64"))
    cs = S1.dot(c) + S1.dot(off) - S1.dot(A.dot(off))
    As = S1 @ A @ np.linalg.inv(S2)

    if cs.ndim != 1:
        cs = cs[0, :]

    return As, cs


# Scale parameters derived from embedded patches to fit original patches
def scale_parameters(param, from_shape, to_shape):
    assert len(from_shape) == 2, "Shape of dimension 2 expected."
    assert len(to_shape) == 2, "Shape of dimension 2 expected."

    alpha1 = to_shape[0] / from_shape[0]
    alpha2 = to_shape[1] / from_shape[1]
    off = 0.0

    param_scaled = np.copy(param)
    for i in range(len(param)):
        for j in range(len(param[0])):
            g, A, c = param[i][j]
            As, cs = scale_transforms(A, c, alpha1, alpha2, off)
            param_scaled[i][j] = g, As, cs

    return param_scaled


class patch_t(Enum):
    full_support = 1
    black_border = 2


# Patches are stored in column-major order (by center-pixel index).
def extract_patches(img, k, v, type=patch_t.black_border):
    assert k % 2 == 1 and v % 2 == 1, "Patches must have uneven size"

    if type == patch_t.full_support:
        (m, n) = img.shape
        patches = np.empty((m - k + 1, n - v + 1, k, v))
        for i in range(m - k + 1):
            for j in range(n - v + 1):
                for i_k in range(k):
                    for j_v in range(v):
                        patches[i, j, i_k, j_v] = img[i + i_k, j + j_v]
    elif type == patch_t.black_border:
        (m, n) = img.shape
        patches = np.zeros((m, n, k, v))
        for j in range(n):
            for i in range(m):
                for i_k in range(k):
                    for j_v in range(v):
                        i_img = i + i_k - k // 2
                        j_img = j + j_v - v // 2
                        if i_img >= 0 and i_img < m and j_img >= 0 and j_img < n:
                            patches[i, j, i_k, j_v] = img[i_img, j_img]
    else:
        assert False, "Patch type not supported."

    return patches


# Embed labels in a black background.
def embed_labels(labels):
    (m, k, v) = labels.shape

    assert k % 2 == 1 and v % 2 == 1, "Labels must have uneven size"

    # The border size is chosen to be big enough, to fit the patch in a maximal circle
    # in the new patch.
    d = int(np.floor(np.sqrt(k * k + v * v)))
    d = d if d % 2 == 1 else d + 1
    off_k = (d - k) // 2
    off_v = (d - v) // 2
    new_labels = np.zeros((m, d, d))
    for i in range(m):
        for i_k in range(off_k, d - off_k):
            for i_v in range(off_v, d - off_v):
                new_labels[i, i_k, i_v] = labels[i, i_k - off_k, i_v - off_v]

    return new_labels


# Embed patches in a black background.
def embed_patches(patches):
    (m, n, k, v) = patches.shape

    assert k % 2 == 1 and v % 2 == 1, "Patches must have uneven size"

    # The border size is chosen to be big enough, to fit the patch in a maximal circle
    # in the new patch.
    d = int(np.floor(np.sqrt(k * k + v * v)))
    d = d if d % 2 == 1 else d + 1
    off_k = (d - k) // 2
    off_v = (d - v) // 2
    new_patches = np.zeros((m, n, d, d))
    for i in range(m):
        for j in range(n):
            for i_k in range(off_k, d - off_k):
                for i_v in range(off_v, d - off_v):
                    new_patches[i, j, i_k, i_v] = patches[
                        i, j, i_k - off_k, i_v - off_v
                    ]

    return new_patches


def resize_patches(patches, k, v, anti_aliasing=True):
    (m, n, _, _) = patches.shape
    new_p = np.empty((m, n, k, v))
    for i in range(m):
        for j in range(n):
            new_p[i, j, :, :] = resize(
                patches[i, j, :, :], (k, v), anti_aliasing)

    return new_p


# Returns a subset of patches.
def get_patches(patches, ind):
    (m, n, k, v) = patches.shape
    if ind.ndim == 1:
        labels = np.empty((ind.size, k, v))
        for i in range(ind.size):
            (k, l) = np.unravel_index(ind[i], (m, n))
            labels[i, :, :] = patches[k, l, :, :]
    else:  # == 2
        labels = np.empty((ind.size, k, v))
        for i in range(ind.size):
            (k, l) = ind[i]
            labels[i, :, :] = patches[k, l, :, :]

    return labels


def greedy_helper(i, j, m, n, func_dist, patches, dist, ind):
    (i_k, i_l) = np.unravel_index(ind[i], (m, n))
    if not np.isin(j, ind):
        (j_k, j_l) = np.unravel_index(j, (m, n))
        d = func_dist(patches[i_k, i_l, :, :], patches[j_k, j_l, :, :])[0]
        return np.min([dist[j], d])
    else:
        return 0.0



def greedy_k_center_impl(patches, k, func_dist, init):
    (m, n, _, _) = patches.shape
    ind = np.empty((k), dtype=int)

    # The first patch is randomly chosen as the center patch.
    ind[0] = init
    dist = np.full((m * n), np.inf)

    for i in range(k - 1):
        # parallel, but slow
        # dist = Parallel(n_jobs=8)(
        #     delayed(greedy_helper)(i, j, m, n, func_dist, patches, dist, ind)
        #     for j in range(m * n)
        # )
        # backup of serialized approach
        (i_k, i_l) = np.unravel_index(ind[i], (m, n))
        for j in range(m * n):
            if not np.isin(j, ind):
                (j_k, j_l) = np.unravel_index(j, (m, n))
                d = func_dist(patches[i_k, i_l, :, :],
                              patches[j_k, j_l, :, :])[0]
                dist[j] = np.min([dist[j], d])
            else:
                dist[j] = 0

        ind[i + 1] = np.argmax(dist)

    return ind


def nn_helper(k, func_dist, labels, patch):
    d_k, g, A, c = func_dist(labels[k], patch)
    return d_k, (g, A, c)


# Returns the indices of the nearest neighbors and the affine parameters.
def nearest_neighbor_impl(patches, labels, func_dist):
    assert patches.ndim == 4  # Patches are stored in the original image dimensions
    (m, n, _, _) = patches.shape  # image shape
    ind = np.empty((m, n), dtype=int)
    param = [[None for i in range(n)] for j in range(m)]

    for i in range(m):
        for j in range(n):
            # parallel, but slow
            # d, param_ij = zip(
            #     *Parallel(n_jobs=8)(
            #         delayed(nn_helper)(k, func_dist, labels, patches[i, j])
            #         for k in range(labels.shape[0])
            #     )
            # )
            # backup of serialized approach
            d = np.empty(labels.shape[0])
            param_ij = [None] * labels.shape[0]
            for k in range(labels.shape[0]):
                d_k, g, A, c = func_dist(labels[k], patches[i, j])
                d[k] = d_k
                param_ij[k] = (g, A, c)

            ind[i, j] = np.argmin(d)
            param[i][j] = param_ij[ind[i, j]]

    return ind, param


# Transforms labels of given indices with given parameters.
def transform_labels(labels, ind, param):
    (m, n) = ind.shape  # image shape
    (k, v) = (labels.shape[1], labels.shape[2])  # patch shape
    affine_labels = np.empty((m, n, k, v))
    if (
        k <= 8 or v <= 8
    ):  # Upsacle image before affine transformation, otherwise artifacts occur.
        param_scaled = scale_parameters(param, (k, v), (16, 16))

    for i in range(m):
        for j in range(n):
            if k <= 8 or v <= 8:
                g, A, c = param_scaled[i][j]
                patch = affine_transform(
                    resize(g * labels[ind[i, j]], (16, 16),
                           anti_aliasing=False), A, c
                )
                affine_labels[i, j] = resize(
                    patch, (k, v,), anti_aliasing=False)
            else:
                g, A, c = param[i][j]
                affine_labels[i, j] = affine_transform(
                    g * labels[ind[i, j]], A, c)

    return affine_labels


# Returns a matrix. Each row contains the projected patch pixels for one image pixel.
# Use embedded patches and crop after transforming (k,v)
def project(labels, k, v, ind, param, type):
    (m, n) = ind.shape  # image shape
    proj = np.zeros((m, n, k, v))

    affine_labels = transform_labels(labels, ind, param)

    if type == patch_t.full_support:  # TODO: Is it interesing?
        proj = proj
    elif type == patch_t.black_border:
        for j in range(n):
            for i in range(m):
                for j_v in range(v):
                    for i_k in range(k):
                        # Indices of center pixels, which patches coincides with a fixed image pixel.
                        i_label = i_k + i - k // 2
                        j_label = j_v + j - v // 2
                        # Patch pixel index which coincides with the image pixel.
                        i_px = k - 1 - i_k
                        j_px = v - 1 - j_v

                        if (
                            i_label >= 0
                            and i_label < m
                            and j_label >= 0
                            and j_label < n
                        ):
                            proj[i, j, i_k, j_v] = affine_labels[
                                i_label, j_label, i_px, j_px
                            ]
    else:
        assert False, "Patch type not supported."

    return proj


class rec_t(Enum):
    median = 1
    mean = 2


# Reconstruct image from labeling
# Use embedded patches and crop after transforming
def reconstruct(
    labels, k, v, ind, param, rec_type=rec_t.median, patch_type=patch_t.black_border
):
    assert patch_type == patch_t.black_border, "Patch type not supported."

    proj = project(labels, k, v, ind, param, patch_type)
    if rec_type == rec_t.median:
        img = np.median(proj, axis=(2, 3))
    elif rec_type == rec_t.mean:
        img = np.mean(proj, axis=(2, 3))
    else:
        assert False, "Reconstruction type not supported."

    return img.reshape(ind.shape)


# ----------
# image : ndarray
#     Input image data. Will be converted to float.
# mode : str
#     One of the following strings, selecting the type of noise to add:

#     'gauss'     Gaussian-distributed additive noise.
#     'poisson'   Poisson-distributed noise generated from the data.
#     's&p'       Replaces random pixels with 0 or 1.
#     'speckle'   Multiplicative noise using out = image + n*image,where
#                 n is uniform noise with specified mean & variance.


def gauss_noise(image, mean=0.0, var=0.1):
    if image.ndim == 2:
        row, col = image.shape
        sigma = var ** 0.5
        gauss = np.random.normal(mean, sigma, (row, col))
        gauss = gauss.reshape(row, col)
        noisy = image + gauss
    else:
        row, col, ch = image.shape
        sigma = var ** 0.5
        gauss = np.random.normal(mean, sigma, (row, col, ch))
        gauss = gauss.reshape(row, col, ch)
        noisy = image + gauss

    return np.clip(noisy, 0.0, 1.0).astype(image.dtype)


# Equivalent to numpy (at least in tested cases)
def least_squares_svd(A, b):
    U, sigma, VT = np.linalg.svd(A)
    Sigma = np.zeros(A.shape)
    k = np.min(A.shape)
    Sigma[:k, :k] = np.diag(sigma)
    (U.dot(Sigma).dot(VT) - A).round(4)
    Sigma_pinv = np.zeros(A.shape).T
    Sigma_pinv[:3, :3] = np.diag(1 / sigma[:3])
    Sigma_pinv.round(3)

    return VT.T.dot(Sigma_pinv).dot(U.T).dot(b)

# SVD for 2x2 matrices
def svd2(m):
    y1, x1 = (m[1, 0] + m[0, 1]), (m[0, 0] - m[1, 1])
    y2, x2 = (m[1, 0] - m[0, 1]), (m[0, 0] + m[1, 1])

    h1 = np.hypot(y1, x1)
    h2 = np.hypot(y2, x2)

    t1 = x1 / h1
    t2 = x2 / h2

    cc = np.sqrt((1 + t1) * (1 + t2))
    ss = np.sqrt((1 - t1) * (1 - t2))
    cs = np.sqrt((1 + t1) * (1 - t2))
    sc = np.sqrt((1 - t1) * (1 + t2))

    c1, s1 = (cc - ss) / 2, (sc + cs) / 2,
    u1 = np.asarray([[c1, -s1], [s1, c1]])

    d = np.asarray([(h1 + h2) / 2, (h1 - h2) / 2])
    sigma = np.diag(d)

    if h1 != h2:
        u2 = np.diag(1 / d).dot(u1.T).dot(m)
    else:
        u2 = np.diag([1 / d[0], 0]).dot(u1.T).dot(m)

    return u1, sigma, u2

# Solves min ||Ax - b|| s.t. x.T * x = I
def procrustes(A, b):
    U, S, VT = np.linalg.svd(b.T @ A, full_matrices=False)

    return (U @ VT).T

# mean squared error
def mse(img1, img2):
    return np.mean(np.abs(img1 - img2) ** 2)

def psnr(img1, img2):
    mse = np.mean(np.abs(img1 - img2) ** 2)
    if mse == 0:
        return 100

    PIXEL_MAX = 1.0
    return 20 * np.log10(PIXEL_MAX / np.sqrt(mse))
