from skimage.io import imread
from skimage.color import rgba2rgb
import matplotlib.pyplot as plt

# RGB images
path_abandoned = "images/abandoned.png"
path_butterfly = "images/butterfly.png"
path_butterfly_orth = "images/butterfly_orth.png"
path_butterfly_rgb = "images/butterfly_rgb.png"
path_butterfly_rgb_orth = "images/butterfly_rgb_orth.png"
path_field = "images/field.png"
path_mandrill = "images/mandrill.png"

img_abandoned = rgba2rgb(imread(path_abandoned)).astype("float32")
img_butterfly_as_rgb = rgba2rgb(
    imread(path_butterfly, as_gray=False)).astype("float32")
img_butterfly_orth_as_rgb = rgba2rgb(
    imread(path_butterfly_orth, as_gray=False)).astype("float32")
img_butterfly_rgb = rgba2rgb(imread(path_butterfly_rgb)).astype("float32")
img_butterfly_rgb_orth = rgba2rgb(
    imread(path_butterfly_rgb_orth)).astype("float32")
img_field = rgba2rgb(imread(path_field)).astype("float32")
img_mandrill = imread(path_mandrill).astype("float32") / 255.0

# Grey images
path_butterfly = "images/butterfly.png"
path_butterfly_orth = "images/butterfly_orth.png"
path_butterfly_affine = "images/butterfly_affine.png"
path_butterfly_sim = "images/butterfly_sim.png"
path_butterfly_noisy = "images/butterfly_noisy.png"
path_butterfly3 = "images/butterfly3.png"
path_fingerprint = "images/fingerprint.png"
path_fingerprint_noisy = "images/fingerprint_noisy.png"
path_white_square = "images/white_on_black.png"

img_butterfly = imread(path_butterfly, as_gray=True).astype("float32")
img_butterfly_orth = imread(
    path_butterfly_orth, as_gray=True).astype("float32")
img_butterfly_affine = imread(
    path_butterfly_affine, as_gray=True).astype("float32")
img_butterfly_sim = imread(
    path_butterfly_sim, as_gray=True).astype("float32")
img_butterfly_noisy = imread(
    path_butterfly_noisy, as_gray=True).astype("float32")
img_butterfly3 = imread(
    path_butterfly3, as_gray=True).astype("float32")
img_fingerprint = imread(
    path_fingerprint, as_gray=True).astype("float32") / 255.0
img_fingerprint_noisy = imread(path_fingerprint_noisy,
                               as_gray=True).astype("float32") / 255.0
img_white_square = imread(path_white_square, as_gray=True).astype("float32")


def store_thesis_images():
    # Store test images for thesis as pdf, so they can be embedded in latex.
    folder = "../figures"
    plt.imsave(folder + "/butterfly.pdf",
               img_butterfly, format="pdf", cmap="gray")
    plt.imsave(
        folder + "/butterfly_orth.pdf", img_butterfly_orth, format="pdf", cmap="gray",
    )
    plt.imsave(
        folder + "/butterfly_affine.pdf", img_butterfly_affine, format="pdf", cmap="gray",
    )
    plt.imsave(
        folder + "/butterfly_sim.pdf", img_butterfly_sim, format="pdf", cmap="gray",
    )
    plt.imsave(
        folder + "/butterfly_noisy.pdf", img_butterfly_noisy, format="pdf", cmap="gray",
    )
    plt.imsave(
        folder + "/butterfly3.pdf", img_butterfly3, format="pdf", cmap="gray",
    )
    plt.imsave(
        folder + "/butterfly_rgb.pdf", img_butterfly_rgb, format="pdf"
    )
    plt.imsave(
        folder + "/butterfly_rgb_orth.pdf", img_butterfly_rgb_orth, format="pdf"
    )
    plt.imsave(folder + "/fingerprint.pdf",
               img_fingerprint, format="pdf", cmap="gray")
    plt.imsave(folder + "/fingerprint_noisy.pdf",
               img_fingerprint_noisy, format="pdf", cmap="gray")
    plt.imsave(folder + "/field.pdf", img_field, format="pdf")


# If images need to be regenerated.
def regenerate_images():
    from scipy.ndimage import affine_transform
    import numpy as np
    import utils as utils

    # butterfly, orthogonal transformation matrix
    A, c = utils.affine_matrix(
        scale=1.0, theta=0.0 * np.pi, phi=0.25 * np.pi, shift=[0, 0],
        img_shape=img_butterfly.shape)
    orth = utils.affine_transform(img_butterfly, A, c)
    plt.imsave("images/butterfly_orth.png", orth, format="png", cmap="gray")

    # affine butterfly
    A, c = utils.affine_matrix(
        scale=1.2, theta=0.1 * np.pi, phi=0.25 * np.pi, shift=[10, 5],
        img_shape=img_butterfly.shape)
    affine = utils.affine_transform(img_butterfly, A, c)
    plt.imsave("images/butterfly_affine.png",
               affine, format="png", cmap="gray")

    # similar butterfly
    A, c = utils.affine_matrix(
        scale=1.2, theta=0.0 * np.pi, phi=0.25 * np.pi, shift=[0, 0],
        img_shape=img_butterfly.shape)
    sim = utils.affine_transform(img_butterfly, A, c)
    plt.imsave("images/butterfly_sim.png", sim, format="png", cmap="gray")

    # translated butterfly
    A, c = utils.affine_matrix(
        scale=1.0, theta=0.0 * np.pi, phi=0.0 * np.pi, shift=[10, 5],
        img_shape=img_butterfly.shape)
    trans = utils.affine_transform(img_butterfly, A, c)
    plt.imsave("images/butterfly_translation.png",
               trans, format="png", cmap="gray")

    # noisy rotated butterfly
    butterfly_noisy = utils.gauss_noise(orth, mean=0.0, var=0.001)
    plt.imsave("images/butterfly_noisy.png",
               butterfly_noisy, format="png", cmap="gray")

    # rotated rgb butterfly
    func = [lambda x, a=th: np.clip(x, 0.0, a)
            for th in np.linspace(0.05, 1.0, 20)]
    A, c = utils.affine_matrix(
        scale=1.0, theta=0.0 * np.pi, phi=0.25 * np.pi, shift=[0, 0],
        img_shape=img_butterfly_rgb.shape)
    butterfly_rgb_orth = utils.affine_transform(img_butterfly_rgb, A, c)
    butterfly_rgb_orth = np.clip(butterfly_rgb_orth, 0, 1)
    plt.imsave("images/butterfly_rgb_orth.png",
               butterfly_rgb_orth, format="png")

    # noisy fingerprint
    fingerprint_noisy = utils.gauss_noise(img_fingerprint, mean=0.0, var=0.05)
    plt.imsave("images/fingerprint_noisy.png",
               fingerprint_noisy, format="png", cmap="gray")
