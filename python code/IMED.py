# %%
import numpy as np
from scipy.signal import convolve2d
from utils import mse


def IMED_naive(img1, img2):
    assert img1.shape == img2.shape, "Images have to be of the same dimension."

    d = 0
    m, n = img1.shape
    for i in range(m):
        for j in range(n):
            for ii in range(m):
                for jj in range(n):
                    d += (
                        np.exp(-0.5 * np.abs((i - ii) *
                                             (i - ii) + (j - jj) * (j - jj)))
                        * (img1[i, j] - img2[i, j])
                        * (img1[ii, jj] - img2[ii, jj])
                    )

    return d


def cft_grey(img):
    h = np.asarray([0.0053, 0.2171, 0.5519, 0.2171, 0.0053])
    H = np.kron(h[:, None], h[None, :])

    return convolve2d(img, H, mode="same")


def cft(img):
    if img.ndim == 2:
        return cft_grey(img)
    else:
        assert img.ndim == 3 and img.shape[2] == 3
        out = np.empty(img.shape)
        out[:, :, 0] = cft(img[:, :, 0])
        out[:, :, 1] = cft(img[:, :, 1])
        out[:, :, 2] = cft(img[:, :, 2])
        return out


def IMED(img1, img2):
    assert img1.shape == img2.shape, "Images have to be of the same dimension."

    h = np.asarray([0.0053, 0.2171, 0.5519, 0.2171, 0.0053])
    H = np.kron(h[:, None], h[None, :])

    if img1.ndim == 2 and img2.ndim == 2:
        img1c = convolve2d(img1, H, mode="same")
        img2c = convolve2d(img2, H, mode="same")
        return np.sqrt(mse(img1c, img2c))
    else:
        assert img1.ndim == 3 and img1.ndim == 3 and img2.shape[2] == 3 and \
            img1.ndim == 3, "The images need to be both gray-scale or rgb."
        img1c = np.empty(img1.shape)
        img1c[:, :, 0] = cft(img1[:, :, 0])
        img1c[:, :, 1] = cft(img1[:, :, 1])
        img1c[:, :, 2] = cft(img1[:, :, 2])
        img2c = np.empty(img2.shape)
        img2c[:, :, 0] = cft(img2[:, :, 0])
        img2c[:, :, 1] = cft(img2[:, :, 1])
        img2c[:, :, 2] = cft(img2[:, :, 2])
        return np.sqrt(mse(img1c, img2c))
