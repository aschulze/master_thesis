# %%
from timeit import default_timer as timer
from cuda_utils import *
from load_images import *
from utils import gauss_noise

folder = "../figures/"

#
##
###
# Thesis Experiments
###
##
#

# %%
plot_error_by_atoms(
    img=img_fingerprint,
    min=10,
    max=500,
    steps=10,
    filename=folder + "fingerprint_error_by_atoms"
)

plot_error_by_atoms(
    img=img_field,
    min=10,
    max=500,
    steps=10,
    filename=folder + "field_error_by_atoms"
)

# %% Also includes procrustes with imed.
plot_error_by_atoms(
    img=img_fingerprint,
    min=10,
    max=500,
    steps=10,
    w_imed=True,
    filename=folder + "fingerprint_error_by_atoms_w_imed"
)

plot_error_by_atoms(
    img=img_field,
    min=10,
    max=500,
    steps=10,
    w_imed=True,
    filename=folder + "field_error_by_atoms_w_imed"
)

#
# fingerprint
#

# %%
plot_aid(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_fingerprint_least_squares",
)

plot_aid_w_translation(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_fingerprint_least_squares_w_translation",
)

plot_aid(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_fingerprint_procrustes",
)

plot_aid_w_translation(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_fingerprint_procrustes_w_translation",
)

plot_aid(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_fingerprint_identity",
)

plot_aid_w_translation(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_fingerprint_identity_w_translation",
)

plot_sid(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_fingerprint",
)

plot_sid_w_translation(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_fingerprint_w_translation",
)

#
# field
#

plot_aid(
    img_rec=img_field,
    img_labels=img_field,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_field_least_squares",
)

plot_aid(
    img_rec=img_field,
    img_labels=img_field,
    patch_shape=p_shape,
    clusters=k,
    solver_labels=pd.solver_t.procrustes,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_field_least_squares_w_proc_labels",
)

plot_aid(
    img_rec=img_field,
    img_labels=img_field,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_field_procrustes",
)

plot_aid_w_translation(
    img_rec=img_field,
    img_labels=img_field,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_field_procrustes_w_translation",
)

plot_aid(
    img_rec=img_field,
    img_labels=img_field,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_field_identity",
)

plot_aid_w_translation(
    img_rec=img_field,
    img_labels=img_field,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_field_identity_w_translation",
)

plot_sid(
    img_rec=img_field,
    img_labels=img_field,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_field",
)

plot_sid_w_aid_labels(
    img_field,
    patch_shape=p_shape,
    log_polar_size=lp,
    clusters=k,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_field_aid_labels",
)

plot_sid_w_translation_w_aid_labels(
    img_field,
    patch_shape=p_shape,
    log_polar_size=lp,
    clusters=k,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_field_w_translation_aid_labels",
)

#
##
###
# Additional experiments
###
##
#

#
# Linen (periodic pattern)
#
# %%
img_linen = imread("images/linen.png", as_gray=True).astype("float32")
plot_aid_w_translation(
    img_rec=img_linen,
    img_labels=img_linen,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
)

# %%
plot_aid_w_translation(
    img_rec=img_linen,
    img_labels=img_linen,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
)

# %%
plot_sid_w_translation(
    img_linen,
    img_linen,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
)

#
# fingerprint with noise
#

# %%
plot_aid(
    img_rec=gauss_noise(img_fingerprint, mean=0.0, var=0.1),
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_fingerprint_noise_least_squares",
)

plot_aid(
    img_rec=gauss_noise(img_fingerprint, mean=0.0, var=0.1),
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_fingerprint_noise_least_squares",
)

# %%
plot_aid(
    img_rec=gauss_noise(img_fingerprint, mean=0.0, var=0.1),
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_fingerprint_noise_procrustes",
)

# %%
plot_sid(
    img_rec=gauss_noise(img_fingerprint, mean=0.0, var=0.1),
    img_labels=img_fingerprint,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_fingerprint_noise",
)

# %%
#
# butterfly
#

plot_aid(
    img_rec=img_butterfly,
    img_labels=img_butterfly,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_butterfly_least_squares",
)

# %%
plot_aid(
    img_rec=img_butterfly,
    img_labels=img_butterfly,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_butterfly_procrustes",
)

# %%
plot_aid_w_translation(
    img_rec=img_butterfly,
    img_labels=img_butterfly,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_butterfly_procrustes_w_translation",
)

# %%
plot_aid(
    img_rec=img_butterfly,
    img_labels=img_butterfly,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_butterfly_identity",
)

# %%
plot_aid_w_translation(
    img_rec=img_butterfly,
    img_labels=img_butterfly,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_butterfly_identity_w_translation",
)

# %%
plot_sid(
    img_rec=img_butterfly,
    img_labels=img_butterfly,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_butterfly",
)

# %%
plot_sid_w_aid_labels(
    img_butterfly,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_butterfly_aid_labels",
)

# %%
plot_sid_w_translation_w_aid_labels(
    img_butterfly,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_butterfly_w_translation_aid_labels",
)

#
# mandrill
#

# %%
plot_aid(
    img_rec=img_mandrill,
    img_labels=img_mandrill,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=True,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_mandrill_east_squares",
)

# %%
plot_aid(
    img_rec=img_mandrill,
    img_labels=img_mandrill,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=True,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_mandrill_procrustes",
)

# %%
plot_aid(
    img_rec=img_mandrill,
    img_labels=img_mandrill,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=True,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_mandrill_identity",
)

# %%
plot_aid_w_sid_labels(
    img=img_mandrill,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=True,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_mandrill_sid_labels",
)

# %%
plot_aid_w_translation_w_sid_labels(
    img=img_mandrill,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=True,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_mandrill_w_translation_sid_labels",
)

# %%
plot_sid(
    img_rec=img_mandrill,
    img_labels=img_mandrill,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_mandrill",
)

#
# abandoned
#

# %%
plot_aid(
    img_rec=img_abandoned,
    img_labels=img_abandoned,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_abandoned_least_squares",
)

# %%
plot_aid(
    img_rec=img_abandoned,
    img_labels=img_abandoned,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_abandoned_procrustes",
)

# %%
plot_aid_w_translation(
    img_rec=img_abandoned,
    img_labels=img_abandoned,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_abandoned_procrustes_w_translation",
)

# %%
plot_aid(
    img_rec=img_abandoned,
    img_labels=img_abandoned,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_abandoned_identity",
)

# %%
plot_aid_w_translation(
    img_rec=img_abandoned,
    img_labels=img_abandoned,
    patch_shape=p_shape,
    clusters=k,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,  # Barely makes a difference.
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "hagege_abandoned_identity_w_translation",
)

# %%
plot_sid(
    img_rec=img_abandoned,
    img_labels=img_abandoned,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_abandoned",
)

# %%
plot_sid_w_translation(
    img_rec=img_abandoned,
    img_labels=img_abandoned,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    interpolation=pd.inter_t.bicubic,
    foldername=folder + "afmt_abandoned_w_translation",
)

#
# white square
#

# %%
plot_sid_w_aid_labels(
    img_white_square,
    patch_shape=p_shape,
    clusters=k,
    log_polar_size=lp,
    fmt_shape=fmt,
    foldername=folder + "afmt_white_square_aid_labels",
    interpolation=pd.inter_t.bicubic,
)

# %% Visualize the transformations for some patches of Mandrill,
# but with disks cut out. (Does not make a difference.)
p_shape = (16, 16)
patch0 = diskify(get_patch(img_mandrill, *p_shape, x=0, y=0))
patch1 = diskify(get_patch(img_mandrill, *p_shape, x=130, y=200))
patch1_ = diskify(get_patch(img_mandrill, *p_shape, x=140, y=200))

plot_aid_images(patch0, patch1, pd.func_t.superlevelsets)
plot_aid_images(patch1_, patch1, pd.func_t.superlevelsets)

# %% Visualize the transformations for some patches of Mandrill.
p_shape = (16, 16)
patch0 = get_patch(img_fingerprint, *p_shape, x=0, y=0)
patch1 = get_patch(img_fingerprint, *p_shape, x=130, y=200)
patch1_ = get_patch(img_fingerprint, *p_shape, x=140, y=200)

plot_aid_images(patch0, patch1, pd.func_t.superlevelsets)
plot_aid_images(patch1_, patch1, pd.func_t.superlevelsets)

# %% Benchmark

start = timer()
sid = pd.similarity_inv_dist()
end = timer()
print("\ninit: {0:.2f}s".format(end - start))

start = timer()
labels, _ = sid.greedy_k_center(
    image=img_field, patch_shape=(9, 9), clusters=16, first_patch_idx=0, log_polar_size=16, fmt_shape=(4, 4), sigma=0.5,
)
end = timer()
print("\ngreedy: {0:.2f}s".format(end - start))

start = timer()
img_rec_median, nn = sid.reconstruct(
    image=img_field,
    labels=labels,
    rec_type=pd.rec_t.Median,
    log_polar_size=16,
    fmt_shape=(4, 4),
    sigma=0.5,
    interpolation=pd.inter_t.bicubic,
)
end = timer()
print("\nreconstruct: {0:.2f}s".format(end - start))

start = timer()
img_rec_median, nn = sid.reconstruct_w_translation(
    image=img_field,
    labels=labels,
    rec_type=pd.rec_t.Median,
    log_polar_size=16,
    fmt_shape=(4, 4),
    sigma=0.5,
    interpolation=pd.inter_t.bicubic,
)
end = timer()
print("\nreconstruct /w translation: {0:.2f}s".format(end - start))

# %%
start = timer()
aid = pd.affine_inv_dist()
end = timer()
print("\ninit: {0:.2f}s".format(end - start))

start = timer()
labels, _ = aid.greedy_k_center(
    image=img_field,
    patch_shape=(9, 9),
    clusters=16,
    first_patch_idx=0,
    solver=pd.solver_t.least_squares,
    func_family=pd.func_t.superlevelsets,
    use_imed=False,
    interpolation=pd.inter_t.bicubic,
)
end = timer()
print("\ngreedy: {0:.2f}s".format(end - start))

start = timer()
img_rec_median, nn = aid.reconstruct(
    image=img_field,
    labels=labels,
    rec_type=pd.rec_t.Median,
    solver=pd.solver_t.least_squares,
    func_family=pd.func_t.superlevelsets,
    use_imed=False,
    interpolation=pd.inter_t.bicubic,
)
end = timer()
print("\nreconstruct: {0:.2f}s".format(end - start))

start = timer()
img_rec_median, nn = aid.reconstruct_w_translation(
    image=img_field,
    labels=labels,
    rec_type=pd.rec_t.Median,
    solver=pd.solver_t.least_squares,
    func_family=pd.func_t.superlevelsets,
    use_imed=False,
    interpolation=pd.inter_t.bicubic,
)
end = timer()
print("\nreconstruct /w translation: {0:.2f}s".format(end - start))

# %%
