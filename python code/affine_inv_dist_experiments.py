# %%
from skimage.io import imread
from skimage.transform import resize
from matplotlib import gridspec
from os import path
from os import mkdir
import matplotlib.pyplot as plt
import numpy as np

from load_images import *
from IMED import cft
import utils as utils
import affine_inv_dist as aid

#
##
### Thesis Experiments (functions)
##
#


def plot_aid_funcs(img1, img2, img3, M, solver, filename=""):
    M = np.linspace(5, 255, M)
    d_clip = np.empty((M.shape))
    d_clip_exp = np.empty((M.shape))
    d_where = np.empty((M.shape))

    for ind, val in enumerate(M):
        clip12 = aid.affine_inv_dist(
            img1,
            img2,
            [lambda x, a=th: np.clip(x, 0.0, a)
             for th in np.linspace(0.0, 1.0, val)],
             solver=solver
        )[0]
        clip_exp12 = aid.affine_inv_dist(
            img1,
            img2,
            [lambda x, a=th: np.exp(np.clip(x, 0.0, a)) - 1
             for th in np.linspace(0.0, 1.0, val)],
            solver=solver
        )[0]
        where12 = aid.affine_inv_dist(
            img1,
            img2,
            [lambda x, a=th: np.where(x > a, 1.0, 0.0)
             for th in np.linspace(0.0, 1.0, val)],
            solver=solver
        )[0]
        clip13 = aid.affine_inv_dist(
            img1,
            img3,
            [lambda x, a=th: np.clip(x, 0.0, a)
             for th in np.linspace(0.0, 1.0, val)],
            solver=solver
        )[0]
        clip_exp13 = aid.affine_inv_dist(
            img1,
            img3,
            [lambda x, a=th: np.exp(np.clip(x, 0.0, a)) - 1
             for th in np.linspace(0.0, 1.0, val)],
            solver=solver
        )[0]
        where13 = aid.affine_inv_dist(
            img1,
            img3,
            [lambda x, a=th: np.where(x > a, 1.0, 0.0)
             for th in np.linspace(0.0, 1.0, val)],
            solver=solver
        )[0]

        d_clip[ind] = clip13 / clip12
        d_clip_exp[ind] = clip_exp13 / clip_exp12
        d_where[ind] = where13 / where12

    plt.figure()
    plt.xlabel(r"Number of functions $M$", fontsize=16)
    plt.ylabel(r"$\frac{d(a,c)}{d(a,b)}$", fontsize=16)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    plt.plot(M, d_where, color="black", linestyle="-", label=r"superlevel sets")
    plt.plot(M, d_clip_exp, color="black", linestyle="-.", label=r"$w_i(x)=e^{min(x,\frac{i}{M})}$")
    plt.plot(M, d_clip, color="black", linestyle=":", label=r"$w_i(x)=min(x,\frac{i}{M})$")
    plt.legend(fontsize="x-large")
    axes = plt.gca()
    axes.set_ylim([0,100])

    if filename != "":
        plt.savefig(filename + ".pdf", format="pdf", bbox_inches='tight')

def diff(img0, img1):
    if img0.ndim == 2 and img1.ndim == 2:
        return np.abs(img0 - img1)
    else:
        assert img0.ndim == 3 and img1.ndim == 3, "The images need to be both gray-scale or rgb."
        return np.linalg.norm(img0-img1, ord=2, axis=2)


def plot_affine_result(img, img_affine, img_estim, img_estim_affine, fname=""):
    plt.figure(figsize=(14, 5))
    plt.subplot(2, 5, 1)
    plt.imshow(img, cmap="gray")
    plt.title("original")
    plt.axis("off")
    plt.subplot(2, 5, 2)
    plt.imshow(img_affine, cmap="gray")
    plt.title("affine")
    plt.axis("off")
    plt.subplot(2, 5, 3)
    plt.imshow(img_estim, cmap="gray")
    plt.title("estimated (first order)")
    plt.axis("off")
    plt.subplot(2, 5, 4)
    plt.imshow(diff(img_affine, img_estim), cmap="Greys")
    plt.title("distance: ")  # + str(img_estim_dist))
    plt.axis("off")
    plt.subplot(2, 5, 5)
    plt.imshow(diff(cft(img_affine), cft(img_estim)), cmap="Greys")
    plt.title("distance (imed): ")  # + str(img_estim_dist))
    plt.axis("off")
    plt.subplot(2, 5, 6)
    plt.imshow(img_affine, cmap="gray")
    plt.title("affine")
    plt.axis("off")
    plt.subplot(2, 5, 7)
    plt.imshow(img, cmap="gray")
    plt.title("original")
    plt.axis("off")
    plt.subplot(2, 5, 8)
    plt.imshow(img_estim_affine, cmap="gray")
    plt.title("estimated (first order)")
    plt.axis("off")
    plt.subplot(2, 5, 9)
    plt.imshow(diff(img, img_estim_affine), cmap="Greys")
    plt.title("distance: ")  # + str(img_estim_affine_dist))
    plt.axis("off")
    plt.subplot(2, 5, 10)
    plt.imshow(diff(cft(img), cft(img_estim_affine)), cmap="Greys")
    plt.title("distance (imed): ")  # + str(img_estim_affine_dist))
    plt.axis("off")

    if not fname == "":
        plt.savefig(fname, dpi=300)


def plot_aid(img1, img2, solver, func, high_order, imed, filename=""):
    # Estimation of img1 by img2.
    gain = utils.gain(img1, img2)
    An, cn, det_An = aid.hagege_affine(img1, img2 / gain, func, high_order, solver)
    An_trans, cn_trans, det_An_trans = aid.hagege_affine(
        img2 / gain, img1, func, high_order, solver)

    img1_ = utils.affine_transform(img1, An, cn)
    img2_ = utils.affine_transform(img2, An_trans, cn_trans)

    plot_affine_result(img1, img2, img1_, img2_)

    if filename != "":
        plt.imsave(filename + "_estim1.pdf",
                   img1_, format="pdf", cmap="gray")
        plt.imsave(filename + "_estim2.pdf",
                   img2_, format="pdf", cmap="gray")
        plt.imsave(filename + "_diff1.pdf",
                   diff(img1_, img2),
                   format="pdf",
                   cmap="Greys"
                   )
        plt.imsave(filename + "_diff2.pdf",
                   diff(img2_, img1),
                   format="pdf",
                   cmap="Greys"
                   )

#
##
### Thesis Experiments
##
#


# %%
# regenerate_images()
# store_thesis_images()

folder = "../figures/"
func = [lambda x, a=th: np.clip(x, 0.0, a) for th in np.linspace(0.05, 1.0, 20)]

# plot_aid_funcs(img_butterfly, img_butterfly_orth, img_butterfly3, 10,
#                solver=aid.solver_t.least_squares, filename=folder+"hagege_func_least_squares")

# plot_aid_funcs(img_butterfly, img_butterfly_orth, img_butterfly3, 10,
#                solver=aid.solver_t.procrustes, filename=folder+"hagege_func_procrustes")

plot_aid(img1=img_butterfly, img2=img_butterfly_affine,
         solver=aid.solver_t.least_squares, func=func, high_order=False, imed=False,
         filename=folder+"hagege_least_squares_affine")
plot_aid(img1=img_butterfly, img2=img_butterfly_orth,
         solver=aid.solver_t.least_squares, func=func, high_order=False, imed=False,
         filename=folder+"hagege_least_squares")
plot_aid(img1=img_butterfly, img2=img_butterfly_orth,
         solver=aid.solver_t.procrustes, func=func, high_order=False, imed=False,
         filename=folder+"hagege_procrustes")
plot_aid(img1=img_butterfly, img2=img_butterfly_orth,
         solver=aid.solver_t.ridge, func=func, high_order=False, imed=False,
         filename=folder+"hagege_ridge")

plot_aid(img1=img_butterfly, img2=img_butterfly_affine,
         solver=aid.solver_t.least_squares, func=func, high_order=True, imed=False,
         filename=folder+"hagege_least_squares_high_affine")

plot_aid(img1=img_butterfly, img2=img_butterfly_orth,
         solver=aid.solver_t.least_squares, func=func, high_order=True, imed=False,
         filename=folder+"hagege_least_squares_high")

plot_aid(img1=img_butterfly, img2=img_butterfly_noisy,
         solver=aid.solver_t.least_squares, func=func, high_order=False, imed=False,
         filename=folder+"hagege_least_squares_noisy")
plot_aid(img1=img_butterfly, img2=img_butterfly_noisy,
         solver=aid.solver_t.procrustes, func=func, high_order=False, imed=False,
         filename=folder+"hagege_procrustes_noisy")

plot_aid(img1=img_butterfly_rgb, img2=img_butterfly_rgb_orth,
         solver=aid.solver_t.least_squares, func=func, high_order=False, imed=False,
         filename=folder+"hagege_least_squares_rgb")

plot_aid(img1=img_butterfly_rgb, img2=img_butterfly_rgb_orth,
         solver=aid.solver_t.procrustes, func=func, high_order=False, imed=False,
         filename=folder+"hagege_procrustes_rgb")

# %%
#
##
# Additional Experiments (not used in the thesis)
##
#


def centroid(img):
    m, n = img.shape
    x = np.tile(np.linspace(-1.0, 1.0, n), m)
    y = np.repeat(np.linspace(-1.0, 1.0, m), n)
    xy = np.concatenate((x.reshape(m * n, 1), y.reshape(m * n, 1)), axis=1)
    centroid = np.sum(img.reshape(
        m * n)[:, None] * xy, axis=0) / np.sum(img, axis=None)

    return (int(((centroid[0] + 1) * m) // 2), int(((centroid[1] + 1) * n) // 2))


def first_moments(img, ws):
    m, n = img.shape
    scale = (img.shape[0] - 1) / 2.0
    offset = 1.0
    mom = compute_moments(img, ws, scale, offset, high_order=False)
    moms = [None] * mom.shape[1]

    for i in range(mom.shape[1]):
        moms[i] = (
            int(((mom[1, i] / mom[0, i] + 1) * m) // 2),
            int(((mom[2, i] / mom[0, i] + 1) * n) // 2),
        )

    return moms


def draw_point(img, p, color):
    for ch in range(len(color)):
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                img[p[0] + i, p[1] + j, ch] = np.min(
                    [color[ch] + img[p[0] + i, p[1] + j, ch], 1.0]
                )

    return img


def plot_moments_image(img1, img2, ws, fname=""):
    assert img1.shape == img2.shape, "Images have to be of the same dimension."

    moments = np.zeros((img1.shape[0], img1.shape[1], 3))
    for w in ws:
        p1 = centroid(w(img1))
        p2 = centroid(w(img2))

        moments = draw_point(moments, p1, [1, 0, 0])
        moments = draw_point(moments, p2, [0, 1, 0])

    plt.figure()
    plt.title("Image moments")
    plt.axis("off")
    plt.imshow(moments)

    if not fname == "":
        plt.savefig(fname, format="pdf")


def plot_moments(img, ws, high_order=False, scale=None, offset=1.0, title="", fname=""):
    # plot as line plot with different functions?
    if scale == None:
        scale = (img.shape[0] - 1) / 2.0

    G = aid.compute_moments(img, ws, scale, offset, high_order)

    plt.figure()
    plt.title(title)
    plt.xlabel(r"order of moments $x^iy^j$")
    plt.ylabel("value")

    if high_order:
        x = [0, 1, 2, 3, 4, 5, 6]
        plt.xticks(x, ["(0,0)", "(1,0)", "(0,1)",
                       "(1,1)", "(1,1)", "(2,0)", "(0,2)"])
    else:
        x = [0, 1, 2]
        plt.xticks(x, ["(0,0)", "(1,0)", "(0,1)"])

    for i in range(len(ws)):
        plt.plot(x, G[:, i], label=r"$w_{}$".format(i))

    plt.legend()

    if not fname == "":
        plt.savefig(fname, format="pdf")


#
# Image Moments
#


# %% Visualize image moments for the butterfly.
img = imread("../unit_tests/images/butterfly.png", as_gray=True)
A, c = utils.affine_matrix(
    scale=1, theta=0.0 * np.pi, phi=0.5 * np.pi, shift=[0, 0], img_shape=img.shape
)
img_affine = utils.affine_transform(img, A, c)
ws = [lambda x, a=th: np.clip(x, 0.0, a) for th in np.linspace(0.2, 1.0, 5)]

aid.plot_moments_image(img, img_affine, ws)

# %% Visualize image moments for the bw.
img = imread("../unit_tests/images/white_on_black.png", as_gray=True)
A, c = utils.affine_matrix(
    scale=1, theta=0.0 * np.pi, phi=0.5 * np.pi, shift=[0, 0], img_shape=img.shape
)
img_affine = utils.affine_transform(img, A, c)
ws = [lambda x, a=th: np.clip(x, 0.0, a) for th in np.linspace(0.2, 1.0, 5)]

aid.plot_moments_image(img, img_affine, ws)

# %% Visualize image moments for the butterfly.
img = imread("../unit_tests/images/butterfly.png", as_gray=True)
A, c = utils.affine_matrix(
    scale=1, theta=0.0 * np.pi, phi=0.5 * np.pi, shift=[0, 0], img_shape=img.shape
)
img_affine = utils.affine_transform(img, A, c)
ws = [lambda x, a=th: np.clip(x, 0.0, a) for th in np.linspace(0.2, 1.0, 5)]

aid.plot_moments(img, ws, True)
aid.plot_moments(img_affine, ws, True)

# %% Visualize image moments for bw.
img = imread("../unit_tests/images/white_on_black.png", as_gray=True)
A, c = utils.affine_matrix(
    scale=1, theta=0.0 * np.pi, phi=0.5 * np.pi, shift=[0, 0], img_shape=img.shape
)
img_affine = utils.affine_transform(img, A, c)
ws = [lambda x, a=th: np.clip(x, 0.0, a) for th in np.linspace(0.2, 1.0, 5)]
ws = [
    lambda x, a=th: np.power(10, np.clip(x, 0.0, a)) - 1
    for th in np.linspace(0.05, 1.0, 5)
]

aid.plot_moments(img, ws, True)
aid.plot_moments(img_affine, ws, True)
