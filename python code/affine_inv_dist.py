from enum import Enum
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import numpy.linalg as nla
import utils as utils
import sklearn.linear_model as lm

import IMED


def is_invertible(a):
    return a.shape[0] == a.shape[1] and np.linalg.matrix_rank(a) == a.shape[0]


class solver_t(Enum):
    least_squares = 1
    least_squares_no_translation = 2
    procrustes = 3
    least_squares_svd = 4
    ridge = 5
    lasso = 6
    elastic = 7
    ransac = 8


# TODO: rectangular patches only
def compute_moments(img, ws, scale, off, high_order=False):
    if img.ndim == 2:
        m, n = img.shape
        X, Y = np.mgrid[0:m:1.0, 0:n:1.0]

        # transform the coordinate system
        X = X / scale - off
        Y = Y / scale - off

        if high_order == True:
            G = np.empty((7, len(ws)))
        else:
            G = np.empty((3, len(ws)))

        # Remark: Trapez, Simpson equal (border conditions have small influence
        # since the images are big enough.)
        for i, w in enumerate(ws):
            G[0, i] = np.sum(w(img))
            G[1, i] = np.sum(X * w(img))
            G[2, i] = np.sum(Y * w(img))
            if high_order == True:
                G[3, i] = np.sum(X * Y * w(img))
                G[4, i] = np.sum(Y * X * w(img))
                G[5, i] = np.sum(X * X * w(img))
                G[6, i] = np.sum(Y * Y * w(img))

        return G
    else:
        m, n, ch = img.shape
        assert ch == 3, "Only color images with three channels are supported."
        X, Y = np.mgrid[0:m:1.0, 0:n:1.0]

        # transform the coordinate system
        X = X / scale - off
        Y = Y / scale - off

        if high_order == True:
            G = np.empty((7, 3 * len(ws)))
        else:
            G = np.empty((3, 3 * len(ws)))

        ch0 = img[:,:,0]
        ch1 = img[:,:,1]
        ch2 = img[:,:,2]

        # Remark: Trapez, Simpson equal (border conditions have small influence
        # since the images are big enough.)
        for i, w in enumerate(ws):
            G[0, 3*i] = np.sum(w(ch0))
            G[1, 3*i] = np.sum(X * w(ch0))
            G[2, 3*i] = np.sum(Y * w(ch0))
            G[0, 3*i+1] = np.sum(w(ch1))
            G[1, 3*i+1] = np.sum(X * w(ch1))
            G[2, 3*i+1] = np.sum(Y * w(ch1))
            G[0, 3*i+2] = np.sum(w(ch1))
            G[1, 3*i+2] = np.sum(X * w(ch1))
            G[2, 3*i+2] = np.sum(Y * w(ch1))
            if high_order == True:
                G[3, 3*i] = np.sum(X * Y * w(ch0))
                G[4, 3*i] = np.sum(Y * X * w(ch0))
                G[5, 3*i] = np.sum(X * X * w(ch0))
                G[6, 3*i] = np.sum(Y * Y * w(ch0))
                G[3, 3*i+1] = np.sum(X * Y * w(ch1))
                G[4, 3*i+1] = np.sum(Y * X * w(ch1))
                G[5, 3*i+1] = np.sum(X * X * w(ch1))
                G[6, 3*i+1] = np.sum(Y * Y * w(ch1))
                G[3, 3*i+2] = np.sum(X * Y * w(ch2))
                G[4, 3*i+2] = np.sum(Y * X * w(ch2))
                G[5, 3*i+2] = np.sum(X * X * w(ch2))
                G[6, 3*i+2] = np.sum(Y * Y * w(ch2))

        return G


def hagege_affine(
    img1, img2, ws, high_order=False, solver=solver_t.least_squares, scale=None, offset=1.0
):  # ws = set of functions
    assert img1.shape == img2.shape, "Images have to be of the same dimension."

    if scale == None:
        scale = (img1.shape[0] - 1) / 2.0

    G = compute_moments(img1, ws, scale, offset, high_order)
    H = compute_moments(img2, ws, scale, offset, high_order)
    det_A = np.dot(G[0], H[0]) / np.dot(H[0], H[0])

    try:
        if solver == solver_t.least_squares:
            T = nla.lstsq(G.T, det_A * H.T, rcond=None)[0][:, 1:].T
        elif solver == solver_t.least_squares_no_translation:
            T = nla.lstsq(G[1:].T, det_A * H[1:].T, rcond=None)[0].T
            A = nla.inv(T)
            center = 0.5 * np.asarray(img1.shape)
            c = center - A.dot(0.5 * np.asarray(img1.shape))
            return A, c, det_A
        elif solver == solver_t.procrustes:
            assert not high_order, "The procrustes solver must be used with low order moments."
            A = utils.procrustes(G[1:].T, det_A * H[1:].T)
            center = 0.5 * np.asarray(img1.shape[0:2])
            c = center - A.dot(0.5 * np.asarray(img1.shape[0:2]))
            return A, c, det_A
        elif solver == solver_t.ridge:
            T = lm.Ridge(alpha=0.5).fit(G.T, det_A * H[1:].T).coef_
        elif solver == solver_t.lasso:
            # TODO: Does not really make sense, i.e. only scale, reflection and rotation by n*pi.
            T = lm.Lasso(alpha=1.0).fit(G[1:].T, det_A * H[1:].T).coef_
        elif solver == solver_t.ransac:
            # TODO: similar results
            T = lm.RANSACRegressor().fit(G.T, det_A * H[1:].T).estimator_.coef_
        else:
            assert False, "Solver type not supported."
    except Exception as e:
        T = np.zeros((2, 3))
        print(e)

    if is_invertible(T[0:2, 1:3]):
        A = nla.inv(T[0:2, 1:3])
        c = -A.dot(T[0:2, 0])
        A, c = utils.scale_transforms(A, c, scale, scale, [offset, offset])
    else:
        A = np.diag([1, 1])
        c = np.zeros(2)

    return A, c, det_A


def affine_inv_dist(
    p1,
    p2,
    ws=[lambda x, a=th: np.clip(x, 0.0, a)
        for th in np.linspace(0.05, 1.0, 20)],
    high_order=False,
    solver=solver_t.least_squares,
    scale=None,
    offset=1.0,
):
    # w_i(x) = min(x, a_i)

    # g = utils.gain(p1, p2) # TODO: This makes the distance asymmetric!
    g = 1.0
    A1, c1, _ = hagege_affine(g * p1, p2, ws, high_order, solver, scale, offset)
    A2, c2, _ = hagege_affine(p2, g * p1, ws, high_order, solver, scale, offset)

    i1 = utils.affine_transform(p1, A1, c1)
    i2 = utils.affine_transform(p2, A2, c2)

    # TODO: Why like this?
    # Symmetrization
    # d1 = sum(sum(abs(p2-i1))).astype(int)
    # d2 = sum(sum(abs(p1-i2))).astype(int)

    # return np.sqrt(d1*d1 + d2*d2), g, A1, c1

    # Symmetrization
    # d1 = np.sqrt(sum(sum(np.square(p2-i1))))
    # d2 = np.sqrt(sum(sum(np.square(p1-i2))))
    d1 = IMED.IMED(p2, i1)  # TODO: test, seems to be better
    d2 = IMED.IMED(p1, i2)

    return 0.5 * (d1 + d2), g, A1, c1


def greedy_k_center(patches, k, init=0):
    emb_patches = utils.embed_patches(patches)

    return utils.greedy_k_center_impl(emb_patches, k, affine_inv_dist, init)


# Returns the indices of the nearest neighbors and the affine parameters.
def nearest_neighbor(patches, labels):
    emb_patches = utils.embed_patches(patches)
    emb_labels = utils.embed_labels(labels)
    ind, param = utils.nearest_neighbor_impl(
        emb_patches, emb_labels, affine_inv_dist)

    return ind, utils.scale_parameters(param, emb_patches.shape[2:], patches.shape[2:])
