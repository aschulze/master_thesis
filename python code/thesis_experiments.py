# %%
#
##
# This file only contains the experiments used in the master thesis.
# All plots are stored in the root folder "figures", with the proper names
# for the latex project.
##
#

import matplotlib.pyplot as plt

from load_images import *
from cuda_utils import *
from utils import gauss_noise
from affine_inv_dist_experiments import *
from sim_inv_dist_experiments import *

store_thesis_images()

#
##
# affine invariant distance
##
#

#
# Experiments on (large) images
#

func = [lambda x, a=th: np.clip(x, 0.0, a)
        for th in np.linspace(0.05, 1.0, 20)]

plot_aid_funcs(img_butterfly, img_butterfly_rot, "../figures/hagege_funcs")

plot_aid(img1=img_butterfly, img2=img_butterfly_rot,
         solver=aid.solver_t.least_squares, func=func, high_order=False, imed=False,
         fname="../figures/hagege_least_squares")
plot_aid(img1=img_butterfly, img2=img_butterfly_rot,
         solver=aid.solver_t.procrustes, func=func, high_order=False, imed=False,
         fname="../figures/hagege_procrustes")
plot_aid(img1=img_butterfly, img2=img_butterfly_rot,
         solver=aid.solver_t.ridge, func=func, high_order=False, imed=False,
         fname="../figures/hagege_ridge")

plot_aid(img1=img_butterfly, img2=img_butterfly_rot,
         solver=aid.solver_t.least_squares, func=func, high_order=True, imed=False,
         fname="../figures/hagege_least_squares_high")
plot_aid(img1=img_butterfly, img2=img_butterfly_rot,
         solver=aid.solver_t.procrustes, func=func, high_order=True, imed=False,
         fname="../figures/hagege_procrustes_high")

plot_aid(img1=img_butterfly, img2=utils.gauss_noise(img_butterfly_rot, mean=0.0, var=0.0001),
         solver=aid.solver_t.least_squares, func=func, high_order=True, imed=False,
         fname="../figures/hagege_least_squares_noise")
plot_aid(img1=img_butterfly, img2=utils.gauss_noise(img_butterfly_rot, mean=0.0, var=0.0001),
         solver=aid.solver_t.procrustes, func=func, high_order=True, imed=False,
         fname="../figures/hagege_procrustes_noise")

#
# Clustering and image reconstruction
#

plot_aid(
    img_rec=img_butterfly,
    img_labels=img_butterfly,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=False,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_butterfly",
)

plot_aid(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=False,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_fingerprint",
)

plot_aid(
    img_rec=gauss_noise(img_fingerprint, mean=0.0, var=0.1),
    img_labels=img_fingerprint,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=True,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_fingerprint_noise",
)

plot_aid(
    img_rec=img_mandrill,
    img_labels=img_mandrill,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.least_squares,
    func=pd.func_t.superlevelsets,
    imed=True,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_mandrill",
)

#
##
# affine invariant distance (orthogonal)
##
#

#
# Clustering and image reconstruction
#

plot_aid(
    img_rec=img_butterfly,
    img_labels=img_butterfly,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_procrustes_butterfly",
)

plot_aid(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=False,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_procrustes_fingerprint",
)

plot_aid(
    img_rec=gauss_noise(img_fingerprint, mean=0.0, var=0.1),
    img_labels=img_fingerprint,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=True,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_procrustes_fingerprint_noise",
)

plot_aid(
    img_rec=img_mandrill,
    img_labels=img_mandrill,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.procrustes,
    func=pd.func_t.superlevelsets,
    imed=True,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_procrustes_mandrill",
)

#
##
# affine invariant distance (identity)
##
#

#
# Clustering and image reconstruction
#

plot_aid(
    img_rec=img_butterfly,
    img_labels=img_butterfly,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_identity_butterfly",
)

plot_aid(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=False,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_identity_fingerprint",
)

plot_aid(
    img_rec=gauss_noise(img_fingerprint, mean=0.0, var=0.1),
    img_labels=img_fingerprint,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=True,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_identity_fingerprint_noise",
)

plot_aid(
    img_rec=img_mandrill,
    img_labels=img_mandrill,
    patch_shape=(7, 7),
    clusters=16,
    solver=pd.solver_t.identity,
    func=pd.func_t.superlevelsets,
    imed=True,
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/hagege_identity_mandrill",
)

#
##
# similarity invariant distance
##
#

#
# Experiments on (large) images
#

plot_alphas(img_butterfly_rot, img_butterfly, 0.75,
            *img_butterfly.shape, fname="../figures/afmt_alpha")
plot_betas(img_butterfly_rot, img_butterfly, -0.25 * np.pi,
           *img_butterfly.shape, fname="../figures/afmt_beta")

plot_sid(img1=img_butterfly, img2=img_butterfly_rot, log_polar_size=128,
         fmt_shape=(4, 4), filename="../figures/afmt_butterfly_rot")
plot_sid(img1=img_butterfly, img2=0.75*img_butterfly_rot, log_polar_size=128,
         fmt_shape=(4, 4), filename="../figures/afmt_butterfly_rot")
plot_sid(img1=img_butterfly, img2=gauss_noise(img_butterfly_rot), log_polar_size=128,
         fmt_shape=(4, 4), filename="../figures/afmt_butterfly_rot")

plot_sid_translation(img, img_sim, img3, fname="../figures/afmt_translation")

plot_sid_fmt_shape(img, img_sim, img3, fname="../figures/afmt_size")

#
# Clustering and image reconstruction
#

plot_sid(
    img_rec=img_butterfly,
    img_labels=img_butterfly,
    patch_shape=(7, 7),
    clusters=16,
    log_polar_size=16,
    fmt_shape=(4, 4),
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/afmt_butterfly",
)

plot_sid_handpicked_labels(
    img_butterfly,
    patch_shape=(7, 7),
    log_polar_size=16,
    fmt_shape=(4, 4),
    interpolation=pd.inter_t.bicubic,
    X=[40, 65, 80],
    Y=[30, 50, 55, 60, 70, 80],
    foldername="../figures/afmt_butterfly_handpicked",
)

plot_sid(
    img_rec=img_fingerprint,
    img_labels=img_fingerprint,
    patch_shape=(7, 7),
    clusters=16,
    log_polar_size=16,
    fmt_shape=(4, 4),
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/afmt_fingerprint",
)

plot_sid(
    img_rec=gauss_noise(img_fingerprint, mean=0.0, var=0.1),
    img_labels=img_fingerprint,
    patch_shape=(7, 7),
    clusters=16,
    log_polar_size=16,
    fmt_shape=(4, 4),
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/afmt_fingerprint_noise",
)

plot_sid(
    img_rec=img_mandrill,
    img_labels=img_mandrill,
    patch_shape=(7, 7),
    clusters=16,
    log_polar_size=16,
    fmt_shape=(4, 4),
    interpolation=pd.inter_t.bicubic,
    foldername="../figures/afmt_mandrill",
)
