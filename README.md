# Master thesis

## Abstract

It is known that a natural image exhibits a property called self-similar, which means that it is
similar to a subset of itself. Hence, it might be enough to know a subset of an image to fully
describe it and perform various image tasks. The goal of this work is to study these patches,
called atoms, in the context of dictionary learning and image reconstruction.

For that purpose,
the main interest lies in the study of image distances to classify patches in their ability to
represent an image. We are particularly interested in distances which are invariant under certain
groups, i.e., the affine group and the similarity group. This ensures less atoms are needed to
describe an image, e.g., by rotating the atom. In this context, the Euclidean distance can be
seen as the trivial reference and consequently is also studied.  Instead of the classic approach
for dictionary learning, which is sparse coding, we are studying k-center clustering using the
aforementioned distances. Images are then recovered by assigning to each image patch an atom and
consecutive averaging of overlapping pixels.

It has been found that, in terms of the mean
squared error, the invariant distances are inferior in reconstructing images compared to the
trivial approach. In particular, dictionary learning was not possible using the similarity
invariant distance. Though, reconstruction works reasonably well from a predefined dictionary.
The same applies to the affine invariant distance. However, restricting this method to orthogonal
matrices yields results, which are similar to the trivial algorithm.

In  conclusion, 
it has been shown that dictionary learning, using the proposed invariant distances, is possible
but inferior to the ordinary Euclidean distance.

## Folder structure

- python code
  - Code to generate the plots in the experiments folder.
  - Depends on [invariant_patch_distances](https://gitlab.com/aschulze/invariant_patch_distances).
- experiments
  - dictionary-learning
  - original images
  - thesis plots